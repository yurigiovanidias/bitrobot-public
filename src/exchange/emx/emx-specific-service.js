const inversify = require("inversify");
const { TYPES } = require("../../utils");

class EMXSpecificService {
    /**
     *
     * @param {EMXClientHttp} emxClient
     */
    constructor(emxClient) {
        this._client = emxClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    contracts(base, quote, perpetual = false) {
        let symbol = `${base}${quote}`

        if (perpetual) {
            symbol = `${symbol}-PERP`;
        }
        const contract = this._client.contracts(symbol);
        return contract;
    }
}

inversify.decorate(inversify.injectable(), EMXSpecificService);
inversify.decorate(inversify.inject(TYPES.EMXClient), EMXSpecificService, 0);

module.exports.EMXSpecificService = EMXSpecificService;
