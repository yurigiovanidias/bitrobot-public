module.exports.EMXClient = require("./emx-client-http").EMXClientHttp;
module.exports.EMXService = require("./emx-service").EMXService;
module.exports.EMXSpecificService = require("./emx-specific-service").EMXSpecificService;
