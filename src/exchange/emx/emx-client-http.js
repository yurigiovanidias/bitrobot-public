const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://api.emx.com";
const API_VERSION = "v1";

class EMXClientHttp extends AbstractExchangeClient {
    /**
     * Method to get specific contract (by code) on EMX
     * @param {string} contract
     */
    async contracts(contract) {
        console.info(`emx-client-http: getting contract for ${contract}`);

        try {
            const res = await this._publicQuery(`contracts/${contract}`, null);
            return res;
        }
        catch (e) {
            console.error(`emx-client-http: error on getting contract for ${contract}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${API_VERSION}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), EMXClientHttp);

module.exports.EMXClientHttp = EMXClientHttp;
