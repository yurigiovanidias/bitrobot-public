const inversify = require("inversify");
const { TYPES } = require("../../utils");
const CONSTANT = require("./constant");
const { Index } = require("../../exchange/type");

class EMXService {
    /**
     *
     * @param {EMXSpecificService} emxSpecificService
     */
    constructor(emxSpecificService) {
        this._emxSpecificService = emxSpecificService;
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<Index>}
     */
    async indexSymbol(symbol) {
        const [base, quote] = symbol.split("_");

        let quoteContract = quote;
        let baseContract = base;

        let promises = [];

        if (base === "BTC" && quote === "BRL") {
            quoteContract = "-PERP";

            promises = [
                this._emxSpecificService.contracts(base, quoteContract, false),
                this._emxSpecificService.contracts("USD", quote, true)
            ];
        }
        else {
            promises = [
                this._emxSpecificService.contracts(base, quote, true),
            ];
        }

        const [resultBase, resultQuote] = await Promise.all(promises);

        let indexPrice = resultBase.index_price;

        if (base === "BTC" && quote === "BRL") {
            indexPrice = +(resultBase.index_price * resultQuote.index_price).toFixed(8);
        }

        return (new Index())
            .setExchange(CONSTANT.ACRONYM)
            .setSymbol(symbol)
            .setPrice(indexPrice)
            .setTimestamp(new Date);
    }
}

inversify.decorate(inversify.injectable(), EMXService);
inversify.decorate(inversify.inject(TYPES.EMXSpecificService), EMXService, 0);

module.exports.EMXService = EMXService
