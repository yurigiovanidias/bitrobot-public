const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type");

class OslService {
    /**
     *
     * @param {OSLSpecificService} oslSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(oslSpecificService, tickerFactory, orderBookFactory) {
        this._oslSpecificService = oslSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    /**
     * Method to get Ticker from OSL
     * @param {string} symbol
     * @return {Promise<Ticker|*>}
     */
    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const tickerOSL = await this._oslSpecificService.ticker(base, quote);
            return this._tickerFactory.create(
                CONSTANT.OSL.ACRONYM,
                symbol,
                tickerOSL.sell.value,
                tickerOSL.buy.value,
                tickerOSL.last.value,
                tickerOSL.high.value,
                tickerOSL.low.value,
                tickerOSL.vol.value,
                tickerOSL.now,
            );
        }
        catch (e) {
            console.error("osl-service: error on getting ticker");
            throw e;
        }
    }

    /**
     *
     * @param {string} symbol
     * @param {number} limit
     * @return {Promise<OrderBook>}
     */
    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookOSL = await this._oslSpecificService.orderBook(base, quote, limit);
            const asks = orderBookOSL.asks.map((ask) => new PriceVolume(ask.unit_price, ask.amount));
            const bids = orderBookOSL.bids.map((bid) => new PriceVolume(bid.unit_price, bid.amount));

            asks.length = limit;
            bids.length = limit;

            return this._orderBookFactory.create(
                CONSTANT.OSL.ACRONYM,
                asks,
                bids,
                new Date(),
            );
        }
        catch (e) {
            console.error("osl-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), OslService);
inversify.decorate(inversify.inject(TYPES.OSLSpecificService), OslService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), OslService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), OslService, 2);

module.exports.OSLService = OslService;
