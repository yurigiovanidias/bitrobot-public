const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://trade-sg.osl.com";
const API_VERSION = "2"
const PUBLIC_PATH = "api"

class OSLClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on OSL from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${quote}${base}`;
        console.info(`osl-client-http: getting ticker for symbol ${symbol}`);

        try {
            const res = await this._publicQuery(`${base}${quote}/money/ticker`, null);

            if (res.result !== "success") {
                throw new Error(res.message);
            }

            return res.data;
        }
        catch (e) {
            console.info(`osl-client-http: error on getting ticker for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     * @return {Promise<*>}
     */
    async orderBook(base, quote, limit) {
        const symbol = `${quote}${base}`;
        console.info(`osl-client-http: getting order book for symbol ${symbol}`);

        try {
            const res = await this._publicQuery(`${symbol}/orders`, {
                limit: limit
            });

            if (res.code !== null) {
                throw new Error(res.message);
            }

            return res.data;
        }
        catch (e) {
            console.info(`osl-client-http: error on getting order book for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * Method to get full URL passing URI
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${PUBLIC_PATH}/${API_VERSION}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), OSLClientHttp);

module.exports.OSLClientHttp = OSLClientHttp;
