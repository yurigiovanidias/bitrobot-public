const inversify = require("inversify");
const { TYPES } = require("../../utils");

class OSLSpecificService {
    /**
     *
     * @param {OSLClientHttp} oslClient
     */
    constructor(oslClient) {
        this._client = oslClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*|undefined>}
     */
    ticker(base, quote) {
        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     * @return {Promise<*|undefined>}
     */
    orderBook(base, quote, limit) {
        const ticker = this._client.orderBook(base, quote, limit);
        return ticker;
    }
}

inversify.decorate(inversify.injectable(), OSLSpecificService);
inversify.decorate(inversify.inject(TYPES.OSLClient), OSLSpecificService, 0);

module.exports.OSLSpecificService = OSLSpecificService;
