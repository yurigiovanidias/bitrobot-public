module.exports.OSLClient = require("./osl-client-http").OSLClientHttp;
module.exports.OSLService = require("./osl-service").OSLService;
module.exports.OSLSpecificService = require("./osl-specific-service").OSLSpecificService;
