const inversify = require("inversify");
const { TYPES } = require("../utils");

const SOURCE = {
    SCHEDULE: "schedule",
    HTTP: "http",
}

class ExchangeManagerWriter {
    /**
     *
     * @param {TickerRepository} tickerRepository
     * @param {OrderBookRepository} orderBookRepository
     * @param {IndexSymbolRepository} indexSymbolRepository
     */
    constructor(indexSymbolRepository, orderBookRepository, tickerRepository) {
        this._indexSymbolRepository = indexSymbolRepository;
        this._orderBookRepository = orderBookRepository;
        this._tickerRepository = tickerRepository;
    }

    /**
     *
     *
     * @param indexesSymbol
     * @param {string} source
     */
    async indexSymbolWrite(indexesSymbol, source = SOURCE.SCHEDULE) {
        this._settingType(indexesSymbol, source);

        return this._indexSymbolRepository.insertMany(indexesSymbol);
    }

    /**
     * Method to write Tickers on repository
     * @param {Ticker[]} tickers
     * @param source
     */
    async tickerWrite(tickers, source = SOURCE.SCHEDULE) {
        this._settingType(tickers, source);

        return this._tickerRepository.insertMany(tickers);
    }

    /**
     * Method to write OrderBooks on repository
     * @param orderBooks
     * @param source
     */
    async orderBookWrite(orderBooks, source = SOURCE.SCHEDULE) {
        this._settingType(orderBooks, source);

        return this._orderBookRepository.insertMany(orderBooks);
    }

    /**
     *
     * @param rows
     * @param source
     * @private
     */
    _settingType(rows, source) {
        rows.forEach(row => row.source = source);
    }
}

inversify.decorate(inversify.injectable(), ExchangeManagerWriter);
inversify.decorate(inversify.inject(TYPES.IndexSymbolRepository), ExchangeManagerWriter, 0);
inversify.decorate(inversify.inject(TYPES.OrderBookRepository), ExchangeManagerWriter, 1);
inversify.decorate(inversify.inject(TYPES.TickerRepository), ExchangeManagerWriter, 2);

module.exports.ExchangeManagerWriter = ExchangeManagerWriter;
