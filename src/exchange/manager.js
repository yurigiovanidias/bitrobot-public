const inversify = require("inversify");
const { TYPES } = require("../utils");

class ExchangeManager {
    /**
     *
     * @param {ExchangeServiceRegistry} exchangeServiceRegistry
     * @param {TickerRepository} tickerRepository
     * @param {OrderBookRepository} orderBookRepository
     * @param {IndexSymbolRepository} indexSymbolRepository
     */
    constructor(exchangeServiceRegistry, indexSymbolRepository, orderBookRepository, tickerRepository) {
        this._exchangeServiceRegistry = exchangeServiceRegistry;
        this._indexSymbolRepository = indexSymbolRepository;
        this._orderBookRepository = orderBookRepository;
        this._tickerRepository = tickerRepository;
    }

    /**
     *
     * @param {string} symbol
     * @param {string[]} acronyms
     * @param {number} minutesDiff
     */
    async indexSymbolRead(symbol, acronyms, minutesDiff = 1) {
        let lastIndexes = [];

        if (minutesDiff > 0) {
            lastIndexes = await this._indexSymbolRepository.getLatestByExchangesAndSymbol(acronyms, symbol, minutesDiff);
        }

        lastIndexes.forEach(index => {
            const key = acronyms.indexOf(index.exchange);

            if (key < 0) {
                return;
            }

            acronyms.splice(key, 1);
        });

        if (acronyms.length <= 0) {
            return [lastIndexes, ];
        }

        const services = this._getExchangeServices(acronyms);

        if (services.length === 0) {
            console.error("exchange-manager: none exchange service registered");
            return;
        }

        const promises = [];
        for (const service of services) {
            if (typeof service.indexSymbol === "undefined") {
                continue;
            }

            promises.push(service.indexSymbol(symbol));
        }

        const result = await Promise.all(promises.map(p => p.catch(e => e)));

        const newIndexes = result.filter((index) => {
            if (typeof index === "undefined" || index.name === Error.name || index.name === TypeError.name) {
                return false;
            }

            return true;
        });

        return [lastIndexes, newIndexes];
    }

    /**
     * Method to get Ticker from all exchanges by acronym
     * @param {string} symbol
     * @param {string[]} acronyms
     * @param {boolean} future
     * @param {number} minutesDiff
     */
    async tickerRead(symbol, acronyms, future, minutesDiff = 1) {
        let lastTickers = [];

        if (minutesDiff > 0) {
            lastTickers = await this._tickerRepository.getLatestByExchangesAndSymbol(acronyms, symbol, minutesDiff);
        }

        lastTickers.forEach(index => {
            const key = acronyms.indexOf(index.exchange);

            if (key < 0) {
                return;
            }

            acronyms.splice(key, 1);
        });

        if (acronyms.length <= 0) {
            return [lastTickers, ];
        }

        const services = this._getExchangeServices(acronyms);

        if (services.length === 0) {
            console.error("exchange-manager: none exchange service registered");
            return;
        }

        const promises = [];
        for (const service of services) {
            if (typeof service.tickerRead === "undefined") {
                continue;
            }

            promises.push(service.tickerRead(symbol, future));
        }

        const result = await Promise.all(promises.map(p => p.catch(e => e)));

        const newTickers = result.filter((ticker) => {
            if (typeof ticker === "undefined" || ticker.name === Error.name || ticker.name === TypeError.name) {
                return false;
            }

            return true;
        });

        return [lastTickers, newTickers];
    }

    /**
     *
     * @param symbol
     * @param acronyms
     * @param limit
     */
    async orderBookRead(symbol, acronyms, limit = 10) {
        const services = this._getExchangeServices(acronyms);

        if (services.length === 0) {
            console.error("exchange-manager: none exchange service registered");
            return;
        }

        const promises = [];
        for (const service of services) {
            if (typeof service.orderBookRead === "undefined") {
                continue;
            }

            promises.push(service.orderBookRead(symbol, limit));
        }

        const result = await Promise.all(promises.map(p => p.catch(e => e)));

        return result.filter((orderBook) => {
            if (typeof orderBook === "undefined" || orderBook.name === Error.name || orderBook.name === TypeError.name) {
                return false;
            }

            if (orderBook.asks.length > limit)
                orderBook.asks.length = limit;

            if (orderBook.bids.length > limit)
                orderBook.bids.length = limit;

            return true;
        });
    }

    /**
     *
     * @param symbol
     * @param interval
     * @param startTime
     * @param endTime
     * @param interval
     * @param acronym
     * @returns {Promise<void>}
     */
    async candleRead(symbol, interval, startTime, endTime, acronym) {
        const service = this._exchangeServiceRegistry.get(acronym);

        if (typeof service === "undefined" || typeof service.candleRead === "undefined") {
            console.error("exchange-manager: none exchange service registered");
            return;
        }

        return service.candleRead(symbol, interval, startTime, endTime)
    }

    /**
     *
     * @param currency
     * @param acronym
     * @returns {Promise<void>}
     */
    async futureContractRead(currency, acronym) {
        const service = this._exchangeServiceRegistry.get(acronym);

        if (typeof service === "undefined" || typeof service.futureContract === "undefined") {
            console.error("exchange-manager: none exchange service registered");
            return;
        }

        return service.futureContract(currency);
    }

    /**
     * Method to get exchange services from desired acronyms
     * @param {string[]} acronyms
     * @return {[]}
     * @private
     */
    _getExchangeServices(acronyms) {
        const services = [];

        if (acronyms.length === 0) {
            const res = this._exchangeServiceRegistry.getItems();

            Object.keys(res).forEach((key) => {
                services.push(res[key]);
            })

            return services;
        }

        for (const acronym of acronyms) {
            const service = this._exchangeServiceRegistry.get(acronym);

            if (typeof service === "undefined") {
                continue;
            }

            services.push(service);
        }

        return services;
    }
}

inversify.decorate(inversify.injectable(), ExchangeManager);
inversify.decorate(inversify.inject(TYPES.ExchangeServiceRegistry), ExchangeManager, 0);
inversify.decorate(inversify.inject(TYPES.IndexSymbolRepository), ExchangeManager, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookRepository), ExchangeManager, 2);
inversify.decorate(inversify.inject(TYPES.TickerRepository), ExchangeManager, 3);

module.exports.ExchangeManager = ExchangeManager;
