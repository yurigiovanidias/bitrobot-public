const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://www.mercadobitcoin.net/api";

class MercadoBitcoinClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on MercadoBitcoin from desired base currency
     * @param {string} base
     */
    async ticker(base) {
        // console.log(`mercadobitcoin-client-http: getting ticker for currency ${base}`);

        try {
            const res = await this._publicQuery(`${base}/ticker`, null);
            return res.ticker;
        }
        catch (e) {
            console.log(`mercacdobitcoin-client-http: error on getting ticker for currency ${base}`);
            throw e;
        }
    }

    /**
     * Method to get order book on MercadoBitcoin from desired base currency
     * @param {string} base
     */
    async orderBook(base) {
        // console.log(`mercadobitcoin-client-http: getting order book for currency ${base}`);

        try {
            const res = await this._publicQuery(`${base}/orderbook`, null);
            return res;
        }
        catch (e) {
            console.log(`mercacdobitcoin-client-http: error on getting order book for currency ${base}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), MercadoBitcoinClientHttp);

module.exports.MercadoBitcoinClientHttp = MercadoBitcoinClientHttp;
