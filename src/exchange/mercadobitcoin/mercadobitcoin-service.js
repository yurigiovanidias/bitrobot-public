const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume, Ticker } = require("../type");

class MercadoBitcoinService {
    /**
     *
     * @param {MercadoBitcoinSpecificService} mercadoBitcoinSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(mercadoBitcoinSpecificService, tickerFactory, orderBookFactory) {
        this._mercadoBitcoinSpecificService = mercadoBitcoinSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<Ticker>}
     */
    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const tickerMercadobitcoin = await this._mercadoBitcoinSpecificService.ticker(base, quote);

            return this._tickerFactory.create(
                CONSTANT.MERCADOBITCOIN.ACRONYM,
                symbol,
                tickerMercadobitcoin.sell,
                tickerMercadobitcoin.buy,
                tickerMercadobitcoin.last,
                tickerMercadobitcoin.high,
                tickerMercadobitcoin.low,
                tickerMercadobitcoin.vol,
                new Date(),
            )
        }
        catch (e) {
            console.error("mercadobitcoin-service: error on getting ticker");
            throw e;
        }
    }

    /**
     *
     * @param {string} symbol
     * @param {number} limit
     * @return {Promise<OrderBook>}
     */
    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookMercadobitcoin = await this._mercadoBitcoinSpecificService.orderBook(base, quote);
            const asks = orderBookMercadobitcoin.asks.map((ask) => new PriceVolume(ask[0], ask[1]) );
            const bids = orderBookMercadobitcoin.bids.map((bid) => new PriceVolume(bid[0], bid[1]) );

            asks.length = limit;
            bids.length = limit;

            return this._orderBookFactory.create(
                CONSTANT.MERCADOBITCOIN.ACRONYM,
                asks,
                bids,
                new Date(),
            )
        }
        catch (e) {
            console.error("mercadobitcoin-service: error on getting order book");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), MercadoBitcoinService);
inversify.decorate(inversify.inject(TYPES.MercadoBitcoinSpecificService), MercadoBitcoinService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), MercadoBitcoinService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), MercadoBitcoinService, 2);

module.exports.MercadoBitcoinService = MercadoBitcoinService;
