module.exports.MercadoBitcoinClient = require("./mercadobitcoin-client-http").MercadoBitcoinClientHttp;
module.exports.MercadoBitcoinService = require("./mercadobitcoin-service").MercadoBitcoinService;
module.exports.MercadoBitcoinSpecificService = require("./mercadobitcoin-specific-service").MercadoBitcoinSpecificService;
