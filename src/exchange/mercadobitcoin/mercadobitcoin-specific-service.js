const inversify = require("inversify");
const { TYPES } = require("../../utils");

class MercadoBitcoinSpecificService {
    /**
     *
     * @param {MercadoBitcoinClientHttp} mercadobitcoinClient
     */
    constructor(mercadobitcoinClient) {
        this._client = mercadobitcoinClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    ticker(base, quote) {
        const ticker = this._client.ticker(base);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    orderBook(base, quote) {
        const orderBook = this._client.orderBook(base);
        return orderBook;
    }
}

inversify.decorate(inversify.injectable(), MercadoBitcoinSpecificService);
inversify.decorate(inversify.inject(TYPES.MercadoBitcoinClient), MercadoBitcoinSpecificService, 0);

module.exports.MercadoBitcoinSpecificService = MercadoBitcoinSpecificService;
