const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type");

class PoloniexService {
    /**
     *
     * @param {PoloniexSpecificService} poloniexSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(poloniexSpecificService, tickerFactory, orderBookFactory) {
        this._poloniexSpecificService = poloniexSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const result = await this._poloniexSpecificService.ticker(base, quote);
            const tickerPoloniex = result[symbol];

            return this._tickerFactory.create(
                CONSTANT.POLONIEX.ACRONYM,
                symbol,
                tickerPoloniex.lowestAsk,
                tickerPoloniex.highestBid,
                tickerPoloniex.last,
                tickerPoloniex.high24hr,
                tickerPoloniex.low24hr,
                tickerPoloniex.baseVolume,
                new Date(),
            )
        } catch (e) {
            console.error("novadax-service: error on getting ticker");
            throw e;
        }
    }

    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookPoloniex = await this._poloniexSpecificService.orderBook(base, quote, limit);
            const asks = orderBookPoloniex.asks.map((ask) => new PriceVolume(ask[0], ask[1]));
            const bids = orderBookPoloniex.bids.map((bid) => new PriceVolume(bid[0], bid[1]));

            return this._orderBookFactory.create(
                CONSTANT.POLONIEX.ACRONYM,
                asks,
                bids,
                new Date(),
            )
        } catch (e) {
            console.error("novadax-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), PoloniexService);
inversify.decorate(inversify.inject(TYPES.PoloniexSpecificService), PoloniexService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), PoloniexService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), PoloniexService, 2);

module.exports.PoloniexService = PoloniexService;
