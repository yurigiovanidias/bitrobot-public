const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://poloniex.com/public";

class PoloniexClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on Poloniex from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${base}_${quote}`;
        console.info(`poloniex-client-http: getting ticker for symbol ${symbol}`);

        try {
            return await this._publicQuery("", {
                command: "returnTicker",
            });
        }
        catch (e) {
            console.error(`poloniex-client-http: error on getting ticker for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * Method to get order book on Poloniex from desired symbol
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     */
    async orderBook(base, quote, limit) {
        const symbol = `${base}_${quote}`;
        console.info(`poloniex-client-http: getting order book for symbol ${symbol}`);

        try {
            const res = await this._publicQuery("", {
                command: "returnOrderBook",
                currencyPair: symbol,
                depth: limit,
            });
            return res;
        }
        catch (e) {
            console.error(`poloniex-client-http: error on getting order book for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     *
     * @return {string}
     * @private
     */
    _getURL() {
        return `${URL}`;
    }
}

inversify.decorate(inversify.injectable(), PoloniexClientHttp);

module.exports.PoloniexClientHttp = PoloniexClientHttp;
