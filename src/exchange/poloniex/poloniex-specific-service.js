const inversify = require("inversify");
const { TYPES } = require("../../utils");

class PoloniexSpecificService {
    /**
     *
     * @param {PoloniexClientHttp} poloniexClient
     */
    constructor(poloniexClient) {
        this._client = poloniexClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    ticker(base, quote) {
        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     */
    orderBook(base, quote, limit) {
        const orderBook = this._client.orderBook(base, quote, limit);
        return orderBook;
    }
}

inversify.decorate(inversify.injectable(), PoloniexSpecificService);
inversify.decorate(inversify.inject(TYPES.PoloniexClient), PoloniexSpecificService, 0);

module.exports.PoloniexSpecificService = PoloniexSpecificService;
