module.exports.PoloniexClient = require("./poloniex-client-http").PoloniexClientHttp;
module.exports.PoloniexService = require("./poloniex-service").PoloniexService;
module.exports.PoloniexSpecificService = require("./poloniex-specific-service").PoloniexSpecificService;
