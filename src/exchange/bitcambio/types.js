class Symbol {
    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    constructor(base, quote) {
        this.base = base;
        this.quote = quote;
    }
}

module.exports.Symbol = Symbol;
