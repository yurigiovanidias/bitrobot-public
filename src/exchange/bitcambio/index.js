module.exports.BitcambioClient = require("./bitcambio-client-http").BitcambioClientHttp;
module.exports.BitcambioService = require("./bitcambio-service").BitcambioService;
module.exports.BitcambioSpecificService = require("./bitcambio-specific-service").BitcambioSpecificService;
