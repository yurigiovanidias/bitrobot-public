const inversify = require("inversify");
const { TYPES } = require("../../utils");

class BitcambioSpecificService {
    /**
     *
     * @param {BitcambioClientHttp} bitcambioClient
     */
    constructor(bitcambioClient) {
        this._client = bitcambioClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    ticker(base, quote) {
        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    orderBook(base, quote) {
        const orderBook = this._client.orderBook(base, quote);
        return orderBook;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    candles(base, quote) {
        const candles = this._client.candles(base, quote);
        return candles;
    }
}

inversify.decorate(inversify.injectable(), BitcambioSpecificService);
inversify.decorate(inversify.inject(TYPES.BitcambioClient), BitcambioSpecificService, 0);

module.exports.BitcambioSpecificService = BitcambioSpecificService
