const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://nova.bitcambio.com.br/api/v3/public";

class BitcambioClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on Bitcambio from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${base}_${quote}`

        console.info(`bitcambio-client-http: getting ticker for symbol ${symbol}`);

        const response = await this.publicQuery(`getticker`, {
            market: symbol,
        });

        return response.find(ticker => {
            return ticker.Market === symbol;
        });
    }

    /**
     * Method to get order book on Bitcambio from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async orderBook(base, quote) {
        const symbol = `${base}_${quote}`;
        console.info(`bitcambio-client-http: getting order book for symbol ${symbol}`);
        return this.publicQuery(`getorderbook`, {
            market: symbol,
        });
    }

    /**
     * Method to get candles on Bitcambio from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async candles(base, quote) {
        const symbol = `${base}_${quote}`;

        console.info(`bitcambio-client-http: getting candles for symbol ${symbol}`);

        return this.publicQuery(`getcandles`, {
            market: symbol,
            period: '1d',
        });
    }

    /**
     *
     * @param {string} uri
     * @protected
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }

    async publicQuery(uri, params) {
        const response = await this._publicQuery(uri, params);

        if (response.success !== true) {
            throw new Error(response.message);
        }

        return response.result;
    }
}

inversify.decorate(inversify.injectable(), BitcambioClientHttp);

module.exports.BitcambioClientHttp = BitcambioClientHttp;
