const inversify = require("inversify");
const { PriceVolume } = require("../type");
const { CONSTANT, TYPES } = require("../../utils");

class BitcambioService {
    /**
     *
     * @param {BitcambioSpecificService} bitcambioSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(bitcambioSpecificService, tickerFactory, orderBookFactory) {
        this._bitcambioSpecificService = bitcambioSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    /**
     * Method to get ticker from Bitcambio
     * @param {string} symbol
     * @return {Promise<Ticker>}
     */
    async tickerRead(symbol) {
        const [base, quote] = symbol.split("_");

        try {
            const [tickerBitcambio, candlesBitcambio] = await Promise.all([
                this._bitcambioSpecificService.ticker(base, quote),
                this._bitcambioSpecificService.candles(base, quote),
            ]);
            return this._tickerFactory.create(
                CONSTANT.BITCAMBIO.ACRONYM,
                symbol,
                tickerBitcambio.Ask,
                tickerBitcambio.Bid,
                tickerBitcambio.Last,
                candlesBitcambio[0].High,
                candlesBitcambio[0].Low,
                candlesBitcambio[0].Volume,
                new Date(),
            );
        }
        catch (e) {
            console.log("bitcambio-service: error on getting ticker");
            throw e;
        }
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<OrderBook>}
     */
    async orderBookRead(symbol) {
        const [base, quote] = symbol.split("_");

        try {
            const orderBookBitcambio = await this._bitcambioSpecificService.orderBook(base, quote);
            const asks = orderBookBitcambio.buy.map((ask) => new PriceVolume(ask.Rate, ask.Quantity));
            const bids = orderBookBitcambio.sell.map((bid) => new PriceVolume(bid.Rate, bid.Quantity));

            return this._orderBookFactory.create(
                CONSTANT.BITCAMBIO.ACRONYM,
                asks,
                bids,
                new Date(),
            );
        }
        catch (e) {
            console.log("bitcambio-service: error on getting ticker");
            throw e;
        }
    }
}

// BUY - BID - SEMPRE MAIOR QUE O SELL
// SELL - ASK

inversify.decorate(inversify.injectable(), BitcambioService);
inversify.decorate(inversify.inject(TYPES.BitcambioSpecificService), BitcambioService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), BitcambioService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), BitcambioService, 2);

module.exports.BitcambioService = BitcambioService
