const inversify = require("inversify");
const { TYPES } = require("../../utils");

class BitprecoSpecificService {
    /**
     *
     * @param {BitPrecoClientHttp} bitPrecoClient
     */
    constructor(bitPrecoClient) {
        this._client = bitPrecoClient;
    }

    /**
     * Method to get get ticker throught bitprecoClient
     * @param {string} base
     * @param {string} quote
     * @return {*}
     */
    async ticker(base, quote) {
        base = base.toLowerCase();
        quote = quote.toLowerCase();

        return this._client.ticker(base, quote);
    }

    /**
     * Method to get order book throught bitprecoClient
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     * @return {*}
     */
    async orderBook(base, quote, limit) {
        base = base.toLowerCase();
        quote = quote.toLowerCase();

        return this._client.orderBook(base, quote, limit);
    }
}

inversify.decorate(inversify.injectable(), BitprecoSpecificService);
inversify.decorate(inversify.inject(TYPES.BitprecoClient), BitprecoSpecificService, 0);

module.exports.BitprecoSpecificService = BitprecoSpecificService;
