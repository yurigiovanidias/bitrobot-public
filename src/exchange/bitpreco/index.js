module.exports.BitprecoClient = require("./bitpreco-client-http").BitprecoClientHttp;
module.exports.BitprecoService = require("./bitpreco-service").BitprecoService;
module.exports.BitprecoSpecificService = require("./bitpreco-specific-service").BitprecoSpecificService;
