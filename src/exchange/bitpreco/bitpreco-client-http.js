const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://api.bitpreco.com";

class BitprecoClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on bitPreco from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${base}-${quote}`
        console.info(`bitpreco-client-http: getting ticker for symbol ${symbol}`);
        return this._publicQuery(`${symbol}/ticker`, null);
    }

    /**
     * Method to get orderBook on bitPreco from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async orderBook(base, quote) {
        const symbol = `${base}-${quote}`
        console.info(`bitpreco-client-http: getting order book for symbol ${symbol}`);
        return this._publicQuery(`${symbol}/orderbook`, null);
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), BitprecoClientHttp);

module.exports.BitprecoClientHttp = BitprecoClientHttp;
