const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type");

class BitprecoService {
    /**
     *
     * @param {BitprecoSpecificService} bitprecoSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(bitprecoSpecificService, tickerFactory, orderBookFactory) {
        this._bitprecoSpecificService = bitprecoSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    /**
     * Method to get ticker throught bitprecoSpecificService
     * @param {string} symbol
     * @return {Promise<Ticker>}
     */
    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const tickerBitpreco = await this._bitprecoSpecificService.ticker(base, quote);
            return this._tickerFactory.create(
                CONSTANT.BITPRECO.ACRONYM,
                symbol,
                tickerBitpreco.buy,
                tickerBitpreco.sell,
                tickerBitpreco.last,
                tickerBitpreco.high,
                tickerBitpreco.low,
                tickerBitpreco.vol,
                new Date(),
            )
        }
        catch (e) {
            console.error("bitpreco-service: error on getting ticker");
            throw e;
        }
    }

    /**
     * Method to get order book throught bitprecoSpecificService
     * @param {string} symbol
     * @param {number} limit
     * @return {Promise<OrderBook>}
     */
    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookBitpreco = await this._bitprecoSpecificService.orderBook(base, quote);
            const asks = orderBookBitpreco.asks.map(ask => new PriceVolume(ask.price, ask.amount) );
            const bids = orderBookBitpreco.bids.map(bid =>  new PriceVolume(bid.price, bid.amount) );

            asks.length = limit;
            bids.length = limit;

            return this._orderBookFactory.create(
                CONSTANT.BITCOINTRADE.ACRONYM,
                asks,
                bids,
                new Date(),
            )
        }
        catch (e) {
            console.error("bitpreco-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), BitprecoService);
inversify.decorate(inversify.inject(TYPES.BitprecoSpecificService), BitprecoService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), BitprecoService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), BitprecoService, 2);

module.exports.BitprecoService = BitprecoService;
