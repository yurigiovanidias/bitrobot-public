const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { CandlestickFactory, Index, PriceVolume } = require("../type");
const { ACRONYM } = require("./constant");

class BinanceService {
    /**
     *
     * @param {BinanceSpecificService} binanceSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(binanceSpecificService, tickerFactory, orderBookFactory) {
        this._binanceSpecificService = binanceSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    /**
     * Method to get ticker throught binanceSpecificService
     * @param {string} symbol
     * @return {Promise<Ticker>}
     */
    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const tickerBinance = await this._binanceSpecificService.ticker(base, quote);
            return this._tickerFactory.create(
                CONSTANT.BINANCE.ACRONYM,
                symbol,
                tickerBinance.askPrice,
                tickerBinance.bidPrice,
                tickerBinance.lastPrice,
                tickerBinance.highPrice,
                tickerBinance.lowPrice,
                tickerBinance.volume,
                new Date(),
            )
        }
        catch (e) {
            console.error("binance-service: error on getting ticker");
            throw e;
        }
    }

    /**
     * Method to get order book throught binanceSpecificService
     * @param {string} symbol
     * @param {number} limit
     * @return {Promise<OrderBook>}
     */
    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookBinance = await this._binanceSpecificService.orderBook(base, quote, limit);
            const asks = orderBookBinance.asks.map(ask => new PriceVolume(ask[0], ask[1]));
            const bids = orderBookBinance.bids.map((bid) => new PriceVolume(bid[0], bid[1]));

            return this._orderBookFactory.create(
                CONSTANT.BINANCE.ACRONYM,
                asks,
                bids,
                new Date(),
            )
        }
        catch (e) {
            console.error("binance-service: error on getting ticker");
            throw e;
        }
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<Index>}
     */
    async indexSymbol(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const avgPriceBinance = await this._binanceSpecificService.indexSymbol(base, quote);
            const index = new Index();

            index.setExchange(ACRONYM)
                .setSymbol(symbol)
                .setPrice(avgPriceBinance.price)
                .setTimestamp(new Date);

            return index;
        }
        catch (e) {
            console.error("binance-service: error on getting avgPrice");
            throw e;
        }
    }

    async candleRead(symbol, interval, startTime, endTime) {
        try {
            const [base, quote] = symbol.split("_");
            const candles = await this._binanceSpecificService.candle(base, quote, interval, startTime, endTime);
            const candlestickFactory = new CandlestickFactory();

            return candles.map(candle => {
                return candlestickFactory.create(
                    ACRONYM,
                    symbol,
                    candle[0],
                    candle[1],
                    candle[2],
                    candle[3],
                    candle[4],
                    candle[5],
                    candle[7],
                    candle[6],
                );
            });
        }
        catch (e) {
            console.error("binance-service: error on getting avgPrice");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), BinanceService);
inversify.decorate(inversify.inject(TYPES.BinanceSpecificService), BinanceService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), BinanceService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), BinanceService, 2);

module.exports.BinanceService = BinanceService;
