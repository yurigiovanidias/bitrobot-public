const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://api.binance.com/api";
const API_VERSION = "v3";

class BinanceClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on binance from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = this._getSymbolBinance(base, quote);
        console.info(`binance-client-http: getting ticker for symbol ${symbol}`);
        return this._publicQuery(`ticker/24hr`, {
            symbol: symbol,
        });
    }

    /**
     * Method to get orderBook on binance from desired symbol
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     */
    async orderBook(base, quote, limit) {
        const symbol = this._getSymbolBinance(base, quote);
        console.info(`binance-client-http: getting order book for symbol ${symbol}`);
        return this._publicQuery(`depth`, {
            symbol: symbol,
            limit: limit,
        });
    }

    /**
     * Method to get avgPrice on binance from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async avgPrice(base, quote) {
        const symbol = this._getSymbolBinance(base, quote);
        console.info(`binance-client-http: getting avg price for symbol ${symbol}`);
        // https://fapi.binance.com/fapi/v1/premiumIndex?symbol=BTCUSDT
        return this._publicQuery(`avgPrice`, {
            symbol: symbol,
        });
    }

    /**
     * Method to get candlesticks on binance from desired symbol
     * @param {string} base
     * @param {string} quote
     * @param {string} interval {1m/5m/15m/30m/1h/4h/12h/1d/1w}
     * @param {string} startTime
     * @param {string} endTime
     */
    async klines(base, quote, interval, startTime, endTime) {
        const symbol = this._getSymbolBinance(base, quote);
        const params = {
            symbol: symbol,
            interval: interval,
            startTime: startTime,
            endTime: endTime,
        };
        console.info(`binance-client-http: getting avg price for symbol ${symbol}`);
        return this._publicQuery(`klines`, params);
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${API_VERSION}/${uri}`;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {string}
     * @private
     */
    _getSymbolBinance(base, quote) {
        return `${base}${quote}`.toUpperCase();
    }
}

inversify.decorate(inversify.injectable(), BinanceClientHttp);

module.exports.BinanceClientHttp = BinanceClientHttp;
