const inversify = require("inversify");
const { TYPES } = require("../../utils");

class BinanceSpecificService {
    /**
     *
     * @param {BinanceClientHttp} binanceClient
     */
    constructor(binanceClient) {
        this._client = binanceClient;
    }

    /**
     * Method to get get ticker throught binanceClient
     * @param {string} base
     * @param {string} quote
     * @return {*}
     */
    async ticker(base, quote) {
        return this._client.ticker(base, quote);
    }

    /**
     * Method to get order book throught binanceClient
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     * @return {*}
     */
    async orderBook(base, quote, limit = 50) {
        return this._client.orderBook(base, quote, limit);
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<Index>}
     */
    async indexSymbol(base, quote) {
        return this._client.avgPrice(base, quote);
    }

    async candle(base, quote, interval, startDate, endTime) {
        return this._client.klines(base, quote, interval, startDate, endTime);
    }
}

inversify.decorate(inversify.injectable(), BinanceSpecificService);
inversify.decorate(inversify.inject(TYPES.BinanceClient), BinanceSpecificService, 0);

module.exports.BinanceSpecificService = BinanceSpecificService;
