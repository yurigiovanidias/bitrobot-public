module.exports.KucoinClient = require("./kucoin-client-http").KucoinClientHttp;
module.exports.KucoinService = require("./kucoin-service").KucoinService;
module.exports.KucoinSpecificService = require("./kucoin-specific-service").KucoinSpecificService;
