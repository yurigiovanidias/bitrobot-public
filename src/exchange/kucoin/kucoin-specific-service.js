const inversify = require("inversify");
const { TYPES } = require("../../utils");

class KucoinSpecificService {
    /**
     *
     * @param {KucoinClientHttp} kucoinClient
     */
    constructor(kucoinClient) {
        this._client = kucoinClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    ticker(base, quote) {
        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     */
    orderBook(base, quote, limit) {
        const orderBook = this._client.orderBook(base, quote, limit);
        return orderBook;
    }
}

inversify.decorate(inversify.injectable(), KucoinSpecificService);
inversify.decorate(inversify.inject(TYPES.KucoinClient), KucoinSpecificService, 0);

module.exports.KucoinSpecificService = KucoinSpecificService;
