const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://api.kucoin.com/api/v1/market";

class KucoinClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on Kucoin from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${base}-${quote}`;
        console.info(`kucoin-client-http: getting ticker for symbol ${symbol}`);

        try {
            const result = await this._publicQuery("allTickers", {
                symbol: symbol,
            });

            return result.data.ticker.find(ticker => ticker.symbol === symbol);
        }
        catch (e) {
            console.error(`kucoin-client-http: error on getting ticker for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * Method to get order book on Kucoin from desired symbol
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     */
    async orderBook(base, quote, limit) {
        const symbol = `${base}-${quote}`;
        console.info(`kucoin-client-http: getting order book for symbol ${symbol}`);

        try {
            const res = await this._publicQuery("orderbook/level2_20", {
                symbol: symbol,
            });
            return res.data;
        }
        catch (e) {
            console.error(`kucoin-client-http: error on getting order book for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), KucoinClientHttp);

module.exports.KucoinClientHttp = KucoinClientHttp;
