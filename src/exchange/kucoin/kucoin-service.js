const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type");

class KucoinService {
    /**
     *
     * @param {KucoinSpecificService} kucoinSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(kucoinSpecificService, tickerFactory, orderBookFactory) {
        this._kucoinSpecificService = kucoinSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const tickerKucoin = await this._kucoinSpecificService.ticker(base, quote);

            return this._tickerFactory.create(
                CONSTANT.KUCOIN.ACRONYM,
                symbol,
                tickerKucoin.sell,
                tickerKucoin.buy,
                tickerKucoin.last,
                tickerKucoin.high,
                tickerKucoin.low,
                tickerKucoin.vol,
                new Date(),
            )
        } catch (e) {
            console.error("kucoin-service: error on getting ticker");
            throw e;
        }
    }

    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookKucoin = await this._kucoinSpecificService.orderBook(base, quote, limit);
            const asks = orderBookKucoin.asks.map((ask) => new PriceVolume(ask[0], ask[1]));
            const bids = orderBookKucoin.bids.map((bid) => new PriceVolume(bid[0], bid[1]));

            return this._orderBookFactory.create(
                CONSTANT.KUCOIN.ACRONYM,
                asks,
                bids,
                new Date(),
            )
        } catch (e) {
            console.error("kucoin-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), KucoinService);
inversify.decorate(inversify.inject(TYPES.KucoinSpecificService), KucoinService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), KucoinService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), KucoinService, 2);

module.exports.KucoinService = KucoinService;
