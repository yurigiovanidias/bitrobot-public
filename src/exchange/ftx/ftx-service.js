const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { Index } = require("../../exchange/type");
const {Ticker, PriceVolume} = require("../type");

class FTXService {
    /**
     *
     * @param {FTXSpecificService} ftxSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     * @param {FutureContractFactory} futureContractFactory
     */
    constructor(ftxSpecificService, tickerFactory, orderBookFactory, futureContractFactory) {
        this._ftxSpecificService = ftxSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
        this._futureContractFactory = futureContractFactory;
    }

    /**
     *
     * @param {string} symbol
     * @param {boolean} future
     * @returns {Promise<Ticker>}
     */
    async tickerRead(symbol, future = false) {
        let [base, quote] = symbol.split("_");
        const result = await this._ftxSpecificService.markets(base, quote, future);

        return this._tickerFactory.create(
            CONSTANT.FTX.ACRONYM,
            symbol,
            result.result.ask,
            result.result.bid,
            result.result.price,
            result.result.price,
            result.result.price,
            result.result.quoteVolume24h,
            new Date
        );
    }

    /**
     *
     * @param {string} symbol
     * @param {number} limit
     * @returns {Promise<OrderBook>}
     */
    async orderBookRead(symbol, limit) {
        const [base, quote] = symbol.split("_");
        const orderBookFTX = await this._ftxSpecificService.orderbook(base, quote, limit);
        const asks = orderBookFTX.result.asks.map((ask) => new PriceVolume(ask[0], ask[1]));
        const bids = orderBookFTX.result.bids.map((bid) => new PriceVolume(bid[0], bid[1]));

        return this._orderBookFactory.create(
            CONSTANT.FTX.ACRONYM,
            asks,
            bids,
            new Date(),
        );
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<Index>}
     */
    async indexSymbol(symbol) {
        const [base, quote] = symbol.split("_");
        let promises = [
            this._ftxSpecificService.markets(base, null, true),
            this._ftxSpecificService.markets(CONSTANT.BRL_SYMBOL, null, true),
        ];

        if (base === "USD" && quote === "BRL") {
            promises = [
                this._ftxSpecificService.markets(CONSTANT.BRL_SYMBOL, null, true),
            ]
        }

        const [resultBase, resultQuote] = await Promise.all(promises);

        let indexPrice = resultBase.result.price;

        if (quote === "BRL" && typeof resultQuote !== "undefined") {
            indexPrice = +(resultBase.result.price / resultQuote.result.price).toFixed(8);
        }

        if (typeof resultBase !== "undefined" && typeof resultQuote === "undefined") {
            indexPrice = +(1 / resultBase.result.price).toFixed(8);
        }

        return (new Index())
            .setExchange(CONSTANT.ACRONYM)
            .setSymbol(symbol)
            .setPrice(indexPrice)
            .setTimestamp(new Date);
    }

    /**
     *
     * @param {string} currency
     * @return {Promise<FutureContract[]>}
     */
    async futureContract(currency) {
        const futures = await this._ftxSpecificService.futures();

        return futures.result.filter(future => {
            return future.underlying === currency && future.type === "future";
        })
        .map(future => {
            return this._futureContractFactory.create(
                CONSTANT.FTX.ACRONYM,
                future.name,
                future.expiry,
                future.description,
                future.expired,
                future.enabled,
            );
        });
    }
}

inversify.decorate(inversify.injectable(), FTXService);
inversify.decorate(inversify.inject(TYPES.FTXSpecificService), FTXService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), FTXService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), FTXService, 2);
inversify.decorate(inversify.inject(TYPES.FutureContractFactory), FTXService, 3);

module.exports.FTXService = FTXService
