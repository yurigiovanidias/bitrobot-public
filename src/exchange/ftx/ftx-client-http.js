const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://ftx.com/api";

class FTXClientHttp extends AbstractExchangeClient {
    /**
     * Method to get specific contract (by code) on FTX
     * @param {string} market
     */
    async markets(market) {
        console.info(`ftx-client-http: getting market for ${market}`);

        try {
            const res = await this._publicQuery(`markets/${market}`, null);
            return res;
        }
        catch (e) {
            console.error(`ftx-client-http: error on getting market for ${market}`);
            throw e;
        }
    }

    async orderbook(market, depth) {
        console.info(`ftx-client-http: getting market orderbook for ${market}`);

        try {
            const res = await this._publicQuery(`markets/${market}/orderbook`, {
                depth: depth,
            });
            return res;
        }
        catch (e) {
            console.error(`ftx-client-http: error on getting market orderbook for ${market}`);
            throw e;
        }
    }

    async futures() {
        console.info(`ftx-client-http: getting futures`);

        try {
            return this._publicQuery(`futures`, null);
        }
        catch (e) {
            console.error(`ftx-client-http: error on getting futures`);
            throw e;
        }
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), FTXClientHttp);

module.exports.FTXClientHttp = FTXClientHttp;
