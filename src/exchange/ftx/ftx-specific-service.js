const inversify = require("inversify");
const { TYPES } = require("../../utils");
const CONSTANT = require("./constant");

class FTXSpecificService {
    /**
     *
     * @param {FTXClientHttp} ftxClient
     */
    constructor(ftxClient) {
        this._client = ftxClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {boolean} future
     */
    markets(base, quote, future) {
        const symbol = this._getSymbol(base, quote, future);
        return this._client.markets(symbol);
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {number} depth
     */
    orderbook(base, quote, depth) {
        const symbol = this._getSymbol(base, quote);
        return this._client.orderbook(symbol);
    }

    futures() {
        return this._client.futures();
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {boolean} isFuture
     * @returns {string}
     * @private
     */
    _getSymbol(base, quote, isFuture) {
        if (isFuture) {
            return `${base}-${quote}`;
        }

        return `${base}/${quote}`
    }
}

inversify.decorate(inversify.injectable(), FTXSpecificService);
inversify.decorate(inversify.inject(TYPES.FTXClient), FTXSpecificService, 0);

module.exports.FTXSpecificService = FTXSpecificService;
