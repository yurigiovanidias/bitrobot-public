const inversify = require("inversify");
const { TYPES } = require("../../utils");

class B2uSpecificService {
    /**
     *
     * @param {B2UClientHttp} b2uClient
     */
    constructor(b2uClient) {
        this._client = b2uClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    ticker(base, quote) {
        if (quote === "BRL") {
            quote = "BRLC";
        }

        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     */
    orderBook(base, quote, limit) {
        if (quote === "BRL") {
            quote = "BRLC";
        }

        const orderBook = this._client.orderBook(base, quote, limit);
        return orderBook;
    }
}

inversify.decorate(inversify.injectable(), B2uSpecificService);
inversify.decorate(inversify.inject(TYPES.B2UClient), B2uSpecificService, 0);

module.exports.B2USpecificService = B2uSpecificService;
