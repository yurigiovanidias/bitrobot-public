const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type");

class B2UService {
    /**
     *
     * @param {B2USpecificService} b2uBitcoinSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(b2uBitcoinSpecificService, tickerFactory, orderBookFactory) {
        this._b2uBitcoinSpecificService = b2uBitcoinSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const [ticker, orderBook] = await Promise.all([
                this._b2uBitcoinSpecificService.ticker(base, quote),
                this.orderBookRead(symbol, 10)
            ]);

            return this._tickerFactory.create(
                CONSTANT.B2U.ACRONYM,
                symbol,
                orderBook.asks[0].price,
                orderBook.bids[0].price,
                ticker.summary.last,
                ticker.summary.high,
                ticker.summary.low,
                ticker.summary.amount,
                new Date(),
            )
        } catch (e) {
            console.error("b2u-service: error on getting ticker");
            throw e;
        }
    }

    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookB2U = await this._b2uBitcoinSpecificService.orderBook(base, quote, limit);
            const asks = orderBookB2U.asks.map((ask) => new PriceVolume(ask[0], ask[1]));
            const bids = orderBookB2U.bids.map((bid) => new PriceVolume(bid[0], bid[1]));

            return this._orderBookFactory.create(
                CONSTANT.B2U.ACRONYM,
                asks,
                bids,
                new Date(),
            )
        } catch (e) {
            console.error("b2u-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), B2UService);
inversify.decorate(inversify.inject(TYPES.B2USpecificService), B2UService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), B2UService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), B2UService, 2);

module.exports.B2UService = B2UService;
