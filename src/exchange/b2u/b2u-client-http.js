const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://back.bitcointoyou.com/API";

class B2UClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on B2U from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${base}_${quote}`;
        console.info(`b2u-client-http: getting ticker for symbol ${symbol}`);

        try {
            return await this._publicQuery("ticker", {
                pair: `${symbol}`,
            });
        }
        catch (e) {
            console.error(`b2u-client-http: error on getting ticker for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * Method to get order book on B2U from desired symbol
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     */
    async orderBook(base, quote, limit) {
        const symbol = `${base}_${quote}`;
        console.info(`b2u-client-http: getting order book for symbol ${symbol}`);

        try {
            return await this._publicQuery("orderbook", {
                pair: `${symbol}`,
                depth: limit
            });
        }
        catch (e) {
            console.error(`b2u-client-http: error on getting order book for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), B2UClientHttp);

module.exports.B2UClientHttp = B2UClientHttp;
