module.exports.AwesomeApiClient = require("./awesome-api-client-http").AwesomeApiClientHttp;
module.exports.AwesomeApiService = require("./awesome-api-service").AwesomeApiService;
module.exports.AwesomeApiSpecificService = require("./awesome-api-specific-service").AwesomeApiSpecificService;
