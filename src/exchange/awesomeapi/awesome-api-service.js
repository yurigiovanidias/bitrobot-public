const inversify = require("inversify");
const { TYPES } = require("../../utils");
const CONSTANT = require("./constant");
const { Index } = require("../../exchange/type");

class AwesomeApiService {
    /**
     *
     * @param {AwesomeApiSpecificService} awesomeApiSpecificService
     */
    constructor(awesomeApiSpecificService) {
        this.awesomeApiSpecificService = awesomeApiSpecificService;
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<Index>}
     */
    async indexSymbol(symbol) {
        const [base, quote] = symbol.split("_");
        let auxBase = base;
        let auxQuote = quote;

        if (auxBase === "USDT") {
            auxBase = "USD"
        }

        if (auxQuote === "USDT") {
            auxQuote = "USD"
        }

        const auxSymbol = `${auxBase}_${auxQuote}`;

        if (auxSymbol !== "BRL_USD" && auxSymbol !== "USD_BRL") {
            throw new Error(`Symbol ${auxSymbol} not allowed`);
        }

        const result = await this.awesomeApiSpecificService.quote(auxBase, auxQuote);

        return (new Index())
            .setExchange(CONSTANT.ACRONYM)
            .setSymbol(symbol)
            .setPrice(result.ask)
            .setTimestamp(new Date);
    }
}

inversify.decorate(inversify.injectable(), AwesomeApiService);
inversify.decorate(inversify.inject(TYPES.AwesomeApiSpecificService), AwesomeApiService, 0);

module.exports.AwesomeApiService = AwesomeApiService
