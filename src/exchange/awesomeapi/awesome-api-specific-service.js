const inversify = require("inversify");
const { TYPES } = require("../../utils");

class AwesomeApiSpecificService {
    /**
     *
     * @param {AwesomeApiClientHttp} awesomeApiClient
     */
    constructor(awesomeApiClient) {
        this._client = awesomeApiClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    async quote(base, quote) {
        const symbol = `${base}-${quote}`
        const result = await this._client.quote(symbol);

        return result[base];
    }
}

inversify.decorate(inversify.injectable(), AwesomeApiSpecificService);
inversify.decorate(inversify.inject(TYPES.AwesomeApiClient), AwesomeApiSpecificService, 0);

module.exports.AwesomeApiSpecificService = AwesomeApiSpecificService;
