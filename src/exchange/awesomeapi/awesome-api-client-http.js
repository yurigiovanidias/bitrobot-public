const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://economia.awesomeapi.com.br/";

class AwesomeApiClientHttp extends AbstractExchangeClient {
    /**
     * Method to get specific quote (by code) on AwesomeAPI
     * @param {string} symbol
     */
    async quote(symbol) {
        console.info(`awesome-api-client-http: getting symbol for ${symbol}`);

        try {
            const res = await this._publicQuery(`all/${symbol}`, null);
            return res;
        }
        catch (e) {
            console.error(`awesome-api-client-http: error on getting symbol for ${symbol}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), AwesomeApiClientHttp);

module.exports.AwesomeApiClientHttp = AwesomeApiClientHttp;
