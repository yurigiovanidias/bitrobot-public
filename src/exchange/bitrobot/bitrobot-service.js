const inversify = require("inversify");
const { TYPES } = require("../../utils");
const CONSTANT = require("./constant");
const { Index } = require("../../exchange/type");

class BitrobotService {
    /**
     *
     * @param {BitprecoService} bitPrecoService
     * @param {BitcointradeService} bitcointradeService
     * @param {MercadoBitcoinService} mercadoBitcoinService
     * @param {TickerFactory} tickerFactory
     */
    constructor(bitPrecoService, bitcointradeService, mercadoBitcoinService, tickerFactory) {
        this._bitPrecoService = bitPrecoService;
        this._bitcointradeService = bitcointradeService;
        this._mercadoService = mercadoBitcoinService;
        this._tickerFactory = tickerFactory;
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<Index>}
     */
    async indexSymbol(symbol) {
        const indexPrice = await this._getAvgPrice(symbol);
        const index = new Index();

        index.setExchange(CONSTANT.ACRONYM)
            .setSymbol(symbol)
            .setPrice(indexPrice)
            .setTimestamp(new Date);

        return index;
    }

    async _getAvgPrice(symbol) {
        const promises = [
            this._bitPrecoService.tickerRead(symbol),
            this._bitcointradeService.tickerRead(symbol),
            this._mercadoService.tickerRead(symbol),
        ];

        const [tickerBPR, tickerBTCT, tickerMB] = await Promise.all(promises);
        const avgBPR = (tickerBPR.bid + tickerBPR.ask) / 2;
        const avgMB = (tickerMB.bid + tickerMB.ask) / 2;
        const avgBTCT = (tickerBTCT.bid + tickerBTCT.ask) / 2;

        return (avgBPR + avgBTCT) / 2;
    }
}

inversify.decorate(inversify.injectable(), BitrobotService);
inversify.decorate(inversify.inject(TYPES.BitprecoService), BitrobotService, 0);
inversify.decorate(inversify.inject(TYPES.BitcointradeService), BitrobotService, 1);
inversify.decorate(inversify.inject(TYPES.MercadoBitcoinService), BitrobotService, 2);
inversify.decorate(inversify.inject(TYPES.TickerFactory), BitrobotService, 3);

module.exports.BitrobotService = BitrobotService
