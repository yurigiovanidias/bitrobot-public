const inversify = require("inversify");
const { TYPES } = require("../../utils");

class BitcointradeSpecificService {
    /**
     *
     * @param {BitcointradeClientHttp} bitcointradeClient
     */
    constructor(bitcointradeClient) {
        this._client = bitcointradeClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*|undefined>}
     */
    ticker(base, quote) {
        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     * @return {Promise<*|undefined>}
     */
    orderBook(base, quote, limit) {
        const ticker = this._client.orderBook(base, quote, limit);
        return ticker;
    }
}

inversify.decorate(inversify.injectable(), BitcointradeSpecificService);
inversify.decorate(inversify.inject(TYPES.BitcointradeClient), BitcointradeSpecificService, 0);

module.exports.BitcointradeSpecificService = BitcointradeSpecificService;
