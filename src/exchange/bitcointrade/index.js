module.exports.BitcointradeClient = require("./bitcointrade-client-http").BitcointradeClientHttp;
module.exports.BitcointradeService = require("./bitcointrade-service").BitcointradeService;
module.exports.BitcointradeSpecificService = require("./bitcointrade-specific-service").BitcointradeSpecificService;
