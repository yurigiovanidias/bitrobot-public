const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://api.bitcointrade.com.br";
const API_VERSION = "v3";
const PUBLIC_PATH = "public"

class BitcointradeClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on Bitcointrade from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${quote}${base}`;
        console.info(`bitcointrade-client-http: getting ticker for symbol ${symbol}`);

        try {
            const res = await this._publicQuery(`${quote}${base}/ticker`, null);

            if (typeof res.code !== "undefined" && res.code !== null) {
                throw new Error(res.message);
            }

            return res.data;
        }
        catch (e) {
            console.info(`bitcointrade-client-http: error on getting ticker for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     * @return {Promise<*>}
     */
    async orderBook(base, quote, limit) {
        const symbol = `${quote}${base}`;
        console.info(`bitcointrade-client-http: getting order book for symbol ${symbol}`);

        try {
            const res = await this._publicQuery(`${symbol}/orders`, {
                limit: limit
            });

            if (typeof res.code !== "undefined" && res.code !== null) {
                throw new Error(res.message);
            }

            return res.data;
        }
        catch (e) {
            console.info(`bitcointrade-client-http: error on getting order book for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * Method to get full URL passing URI
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${API_VERSION}/${PUBLIC_PATH}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), BitcointradeClientHttp);

module.exports.BitcointradeClientHttp = BitcointradeClientHttp;
