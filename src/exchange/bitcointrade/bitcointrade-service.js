const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type");

class BitcointradeService {
    /**
     *
     * @param {BitcointradeSpecificService} bitcointradeSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(bitcointradeSpecificService, tickerFactory, orderBookFactory) {
        this._bitcointradeSpecificService = bitcointradeSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    /**
     * Method to get Ticker from Bitcointrade
     * @param {string} symbol
     * @return {Promise<Ticker|*>}
     */
    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const tickerBitcointrade = await this._bitcointradeSpecificService.ticker(base, quote);
            return this._tickerFactory.create(
                CONSTANT.BITCOINTRADE.ACRONYM,
                symbol,
                tickerBitcointrade.sell,
                tickerBitcointrade.buy,
                tickerBitcointrade.last,
                tickerBitcointrade.high,
                tickerBitcointrade.low,
                tickerBitcointrade.volume,
                new Date(),
            );
        }
        catch (e) {
            console.error("bitcointrade-service: error on getting ticker");
            throw e;
        }
    }

    /**
     *
     * @param {string} symbol
     * @param {number} limit
     * @return {Promise<OrderBook>}
     */
    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookBitcointrade = await this._bitcointradeSpecificService.orderBook(base, quote, limit);
            const asks = orderBookBitcointrade.asks.map((ask) => new PriceVolume(ask.unit_price, ask.amount));
            const bids = orderBookBitcointrade.bids.map((bid) => new PriceVolume(bid.unit_price, bid.amount));

            asks.length = limit;
            bids.length = limit;

            return this._orderBookFactory.create(
                CONSTANT.BITCOINTRADE.ACRONYM,
                asks,
                bids,
                new Date(),
            );
        }
        catch (e) {
            console.error("bitcointrade-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), BitcointradeService);
inversify.decorate(inversify.inject(TYPES.BitcointradeSpecificService), BitcointradeService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), BitcointradeService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), BitcointradeService, 2);

module.exports.BitcointradeService = BitcointradeService;
