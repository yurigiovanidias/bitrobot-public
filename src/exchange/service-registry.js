const inversify = require("inversify");

class ExchangeServiceRegistry {
    constructor() {
        this._items = {};
    }

    /**
     * Method to register an ExchangeService by acronym
     * @param {string} acronym
     * @param exchangeService
     * @return {ExchangeServiceRegistry}
     */
    add(acronym, exchangeService) {
        this._items[acronym] = exchangeService;
        return this;
    }

    /**
     * Method to get a registered ExchangeService by acronym
     * @param acronym
     * @return {*}
     */
    get(acronym) {
        const exchangeService = this._items[acronym];

        if (typeof exchangeService !== "undefined") {
            return exchangeService;
        }
    }

    /**
     * Method to get all registered ExchangeService
     * @return {ExchangeService[]}
     */
    getItems() {
        return this._items;
    }
}

inversify.decorate(inversify.injectable(), ExchangeServiceRegistry);

module.exports.ExchangeServiceRegistry = ExchangeServiceRegistry;
