module.exports.BitmexClient = require("./bitmex-client-http").BitmexClientHttp;
module.exports.BitmexService = require("./bitmex-service").BitmexService;
module.exports.BitmexSpecificService = require("./bitmex-specific-service").BitmexSpecificService;
