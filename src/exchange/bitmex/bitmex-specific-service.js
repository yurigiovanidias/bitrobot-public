const inversify = require("inversify");
const { TYPES } = require("../../utils");
const CONSTANT  = require("./constant");

class BitmexSpecificService {
    /**
     *
     * @param {BitmexClientHttp} bitmexClient
     */
    constructor(bitmexClient) {
        this._client = bitmexClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    async instrument(base, quote) {
        return this._client.instrument(`${base}${quote}`);
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    async instrumentIndices(base, quote) {
        const res = await this._client.instrumentIndices();
        let symbol = `.${base}`;

        if (base !== CONSTANT.BTC) {
            symbol = `.B${base}`;
        }

        return res.find((index) => {
            return index.symbol === symbol;
        })
    }
}

inversify.decorate(inversify.injectable(), BitmexSpecificService);
inversify.decorate(inversify.inject(TYPES.BitmexClient), BitmexSpecificService, 0);

module.exports.BitmexSpecificService = BitmexSpecificService
