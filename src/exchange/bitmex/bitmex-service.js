const inversify = require("inversify");
const { TYPES } = require("../../utils");
const CONSTANT = require("./constant");
const { Index } = require("../../exchange/type");

class BitmexService {
    /**
     *
     * @param {BitmexSpecificService} bitmexSpecificService
     * @param {AwesomeApiSpecificService} awesomeApiService
     * @param {TickerFactory} tickerFactory
     */
    constructor(bitmexSpecificService, awesomeApiService, tickerFactory) {
        this._bitmexSpecificService = bitmexSpecificService;
        this._awesomeApiSpecificService = awesomeApiService;
        this._tickerFactory = tickerFactory;
    }

    async indexSymbol(symbol) {
        let [base, quote] = symbol.split("_");
        const promises = [];

        if (base === "BTC") {
            base = CONSTANT.BTC;
        }

        let quoteBitmex = quote;
        if (quote === "BRL") {
            quoteBitmex = "USD";
        }

        promises.push(this._bitmexSpecificService.instrumentIndices(base, quoteBitmex));

        let promiseAwesomeApi = null;

        if (quote === "BRL") {
            base = "USD";

            promiseAwesomeApi = this._awesomeApiSpecificService.quote(base, quote);
        }

        promises.push(promiseAwesomeApi);

        const [resBitmex, resAwesomeApi] = await Promise.all(promises);
        const index = new Index();

        let indexPrice = resBitmex.lastPrice;

        if (resAwesomeApi) {
            indexPrice = resBitmex.lastPrice * resAwesomeApi.ask;
        }

        index.setExchange(CONSTANT.ACRONYM)
            .setSymbol(symbol)
            .setPrice(indexPrice)
            .setTimestamp(new Date);

        return index;
    }

    /**
     * Method to get ticker from desired symbol Bitmex
     * @param {string} symbol
     * @return {Promise<Ticker>}
     */
    async tickerRead(symbol) {
        const [base, quote] = symbol.split("_");

        try {
            const tickerBitmex = await this._bitmexSpecificService.instrument(base, quote);

            return this._tickerFactory.create(
                CONSTANT.BITMEX.ACRONYM,
                symbol,
                tickerBitmex.askPrice,
                tickerBitmex.bidPrice,
                tickerBitmex.lastPrice,
                tickerBitmex.highPrice,
                tickerBitmex.lowPrice,
                tickerBitmex.volume,
                new Date(),
            )
        }
        catch (e) {
            console.error("bitmex-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), BitmexService);
inversify.decorate(inversify.inject(TYPES.BitmexSpecificService), BitmexService, 0);
inversify.decorate(inversify.inject(TYPES.AwesomeApiSpecificService), BitmexService, 1);
inversify.decorate(inversify.inject(TYPES.TickerFactory), BitmexService, 2);

module.exports.BitmexService = BitmexService
