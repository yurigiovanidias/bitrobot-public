const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://www.bitmex.com/api";
const API_VERSION = "v1";

class BitmexClientHttp extends AbstractExchangeClient {
    /**
     * Method to get instrument on Bitmex from desired symbol
     * @param {string} symbol
     */
    async instrument(symbol) {
        console.info(`bitmex-client-http: getting instrument for symbol ${symbol}`);

        const res = await this._publicQuery("instrument", {
            symbol: `${symbol}`,
        });

        return res.find((instrument) => {
            return instrument.symbol === symbol;
        })
    }

    /**
     * Method to get instrument indeces on Bitmex from desired symbol
     */
    async instrumentIndices() {
        console.info(`bitmex-client-http: getting instrumentIndices`);

        return this._publicQuery("instrument/indices", null);
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${API_VERSION}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), BitmexClientHttp);

module.exports.BitmexClientHttp = BitmexClientHttp;
