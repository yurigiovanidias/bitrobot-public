const inversify = require("inversify");
const { TYPES } = require("../../utils");

class BithumbSpecificService {
    /**
     *
     * @param {BithumbClientHttp} bithumbClient
     */
    constructor(bithumbClient) {
        this._client = bithumbClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    ticker(base, quote) {
        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    orderBook(base, quote) {
        const orderBook = this._client.orderBook(base, quote);
        return orderBook;
    }
}

inversify.decorate(inversify.injectable(), BithumbSpecificService);
inversify.decorate(inversify.inject(TYPES.BithumbClient), BithumbSpecificService, 0);

module.exports.BithumbSpecificService = BithumbSpecificService;
