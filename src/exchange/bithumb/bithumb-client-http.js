const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://global-openapi.bithumb.pro/openapi/v1";

class BithumbClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on Bithumb from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${base}-${quote}`;
        console.info(`bithumb-client-http: getting ticker for symbol ${symbol}`);

        try {
            const result = await this._publicQuery("spot/ticker", {
                symbol: symbol,
            });
            return result.data.find(result => result.s === symbol);
        }
        catch (e) {
            console.error(`bithumb-client-http: error on getting ticker for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * Method to get order book on Bithumb from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async orderBook(base, quote) {
        const symbol = `${base}-${quote}`;
        console.info(`bithumb-client-http: getting order book for symbol ${symbol}`);

        try {
            const res = await this._publicQuery("spot/orderBook", {
                symbol: symbol,
            });
            return res;
        }
        catch (e) {
            console.error(`bithumb-client-http: error on getting order book for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), BithumbClientHttp);

module.exports.BithumbClientHttp = BithumbClientHttp;
