const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type");

class BithumbService {
    /**
     *
     * @param {BithumbSpecificService} bithumbSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(bithumbSpecificService, tickerFactory, orderBookFactory) {
        this._bithumbSpecificService = bithumbSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const [tickerBithumb, orderBook] = await Promise.all([
                this._bithumbSpecificService.ticker(base, quote),
                this.orderBookRead(symbol, 10),
            ]);

            return this._tickerFactory.create(
                CONSTANT.BITHUMB.ACRONYM,
                symbol,
                orderBook.asks[0].price,
                orderBook.bids[0].price,
                tickerBithumb.c,
                tickerBithumb.h,
                tickerBithumb.l,
                tickerBithumb.vol,
                new Date(),
            )
        } catch (e) {
            console.error("novadax-service: error on getting ticker");
            throw e;
        }
    }

    async orderBookRead(symbol, limit) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookBithumb = await this._bithumbSpecificService.orderBook(base, quote);
            const asks = orderBookBithumb.data.s.map((ask) => new PriceVolume(ask[0], ask[1]));
            const bids = orderBookBithumb.data.b.map((bid) => new PriceVolume(bid[0], bid[1]));

            asks.length = limit;
            bids.length = limit;

            return this._orderBookFactory.create(
                CONSTANT.BITHUMB.ACRONYM,
                asks,
                bids,
                new Date(),
            )
        } catch (e) {
            console.error("novadax-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), BithumbService);
inversify.decorate(inversify.inject(TYPES.BithumbSpecificService), BithumbService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), BithumbService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), BithumbService, 2);

module.exports.BithumbService = BithumbService;
