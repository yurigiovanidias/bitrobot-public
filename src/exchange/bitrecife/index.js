module.exports.BitRecifeClient = require("./bitrecife-client-http").BitRecifeClientHttp;
module.exports.BitRecifeService = require("./bitrecife-service").BitRecifeService;
module.exports.BitRecifeSpecificService = require("./bitrecife-specific-service").BitRecifeSpecificService;
