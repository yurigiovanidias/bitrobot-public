const inversify = require("inversify");
const { TYPES } = require("../../utils");

class BitrecifeSpecificService {
    /**
     *
     * @param {BitRecifeClientHttp} bitRecifeClient
     */
    constructor(bitRecifeClient) {
        this._client = bitRecifeClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    async ticker(base, quote) {
        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     * @return {Promise<*>}
     */
    async orderBook(base, quote) {
        const orderBook = this._client.orderBook(base, quote);
        return orderBook;
    }
}

inversify.decorate(inversify.injectable(), BitrecifeSpecificService);
inversify.decorate(inversify.inject(TYPES.BitRecifeClient), BitrecifeSpecificService, 0);

module.exports.BitRecifeSpecificService = BitrecifeSpecificService;
