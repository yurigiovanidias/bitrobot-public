const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type.js");

class BitrecifeService {
    /**
     *
     * @param {BitRecifeSpecificService} bitReficeSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(bitReficeSpecificService, tickerFactory, orderBookFactory) {
        this._bitReficeSpecificService = bitReficeSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<Ticker>}
     */
    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const tickerBitrecife = await this._bitReficeSpecificService.ticker(base, quote);

            return this._tickerFactory.create(
                CONSTANT.BITRECIFE.ACRONYM,
                symbol,
                tickerBitrecife.Ask,
                tickerBitrecife.Bid,
                tickerBitrecife.Last,
                tickerBitrecife.high,
                tickerBitrecife.low,
                tickerBitrecife.vol,
                new Date(),
            );
        }
        catch (e) {
            console.error("bitrecife-service: error on getting ticker");
            throw e;
        }
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<OrderBook>}
     */
    async orderBookRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookBitrecife = await this._bitReficeSpecificService.orderBook(base, quote);
            const asks = orderBookBitrecife.buy.map((ask) => new PriceVolume(ask.Rate, ask.Quantity));
            const bids = orderBookBitrecife.sell.map((bid) => new PriceVolume(bid.Rate, bid.Quantity));

            return this._orderBookFactory.create(
                CONSTANT.BITRECIFE.ACRONYM,
                asks,
                bids,
                new Date(),
            );
        }
        catch (e) {
            console.error("bitrecife-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), BitrecifeService);
inversify.decorate(inversify.inject(TYPES.BitRecifeSpecificService), BitrecifeService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), BitrecifeService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), BitrecifeService, 2);

module.exports.BitRecifeService = BitrecifeService;
