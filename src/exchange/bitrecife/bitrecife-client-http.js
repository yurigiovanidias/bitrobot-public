const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://exchange.bitrecife.com.br/api";
const API_VERSION = "v3";
const PUBLIC_PATH = "public"

class BitrecifeClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on BitRecife from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${base}_${quote}`;

        console.info(`bitrecife-client-http: getting ticker for symbol ${symbol}`);

        try {
            const res = await this._publicQuery("getticker", {
                market: symbol,
            });

            if (res.success !== true) {
                throw new Error(res.message);
            }

            return res.result.find((ticker => ticker.Market === symbol));
        }
        catch (e) {
            console.error(`bitrecife-client-http: error on getting ticker for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * Method to get ticker on BitRecife from desired symbol
     * @param {string} base
     * @param {string} quote
     * @param {number} depth
     */
    async orderBook(base, quote, depth) {
        const symbol = `${base}_${quote}`;

        console.info(`bitrecife-client-http: getting ticker for symbol ${symbol}`);

        try {
            const res = await this._publicQuery("getorderbook", {
                market: symbol,
                type: "ALL", // buy, sell or all
                depth: depth,
            });

            if (res.success !== true) {
                throw new Error(res.message);
            }

            return res.result;
        }
        catch (e) {
            console.error(`bitrecife-client-http: error on getting order book for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${API_VERSION}/${PUBLIC_PATH}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), BitrecifeClientHttp);

module.exports.BitRecifeClientHttp = BitrecifeClientHttp;
