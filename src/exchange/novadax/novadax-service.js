const inversify = require("inversify");
const { CONSTANT, TYPES } = require("../../utils");
const { PriceVolume } = require("../type");

class NovaDaxService {
    /**
     *
     * @param {NovaDaxSpecificService} novadaxBitcoinSpecificService
     * @param {TickerFactory} tickerFactory
     * @param {OrderBookFactory} orderBookFactory
     */
    constructor(novadaxBitcoinSpecificService, tickerFactory, orderBookFactory) {
        this._novadaxBitcoinSpecificService = novadaxBitcoinSpecificService;
        this._tickerFactory = tickerFactory;
        this._orderBookFactory = orderBookFactory;
    }

    async tickerRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const tickerNovadax = await this._novadaxBitcoinSpecificService.ticker(base, quote);

            return this._tickerFactory.create(
                CONSTANT.NOVADAX.ACRONYM,
                symbol,
                tickerNovadax.ask,
                tickerNovadax.bid,
                tickerNovadax.lastPrice,
                tickerNovadax.high24h,
                tickerNovadax.low24h,
                tickerNovadax.baseVolume24h,
                new Date(),
            )
        } catch (e) {
            console.error("novadax-service: error on getting ticker");
            throw e;
        }
    }

    async orderBookRead(symbol) {
        try {
            const [base, quote] = symbol.split("_");
            const orderBookNovadax = await this._novadaxBitcoinSpecificService.orderBook(base, quote);
            const asks = orderBookNovadax.asks.map((ask) => new PriceVolume(ask[0], ask[1]));
            const bids = orderBookNovadax.bids.map((bid) => new PriceVolume(bid[0], bid[1]));

            return this._orderBookFactory.create(
                CONSTANT.NOVADAX.ACRONYM,
                asks,
                bids,
                new Date(),
            )
        } catch (e) {
            console.error("novadax-service: error on getting ticker");
            throw e;
        }
    }
}

inversify.decorate(inversify.injectable(), NovaDaxService);
inversify.decorate(inversify.inject(TYPES.NovaDaxSpecificService), NovaDaxService, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), NovaDaxService, 1);
inversify.decorate(inversify.inject(TYPES.OrderBookFactory), NovaDaxService, 2);

module.exports.NovaDaxService = NovaDaxService;
