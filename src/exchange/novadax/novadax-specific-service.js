const inversify = require("inversify");
const { TYPES } = require("../../utils");

class NovaDaxSpecificService {
    /**
     *
     * @param {NovaDaxClientHttp} novadaxClient
     */
    constructor(novadaxClient) {
        this._client = novadaxClient;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    ticker(base, quote) {
        const ticker = this._client.ticker(base, quote);
        return ticker;
    }

    /**
     *
     * @param {string} base
     * @param {string} quote
     */
    orderBook(base, quote) {
        const orderBook = this._client.orderBook(base, quote);
        return orderBook;
    }
}

inversify.decorate(inversify.injectable(), NovaDaxSpecificService);
inversify.decorate(inversify.inject(TYPES.NovaDaxClient), NovaDaxSpecificService, 0);

module.exports.NovaDaxSpecificService = NovaDaxSpecificService;
