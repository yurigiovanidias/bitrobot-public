const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "api.novadax.com";
const API_VERSION = "v1";

class NovaDaxClientHttp extends AbstractExchangeClient {
    /**
     * Method to get ticker on NovaDax from desired symbol
     * @param {string} base
     * @param {string} quote
     */
    async ticker(base, quote) {
        const symbol = `${base}_${quote}`;
        console.info(`novadax-client-http: getting ticker for symbol ${symbol}`);

        try {
            const res = await this._publicQuery("market/ticker", {
                symbol: `${symbol}`,
            });
            return res.data;
        }
        catch (e) {
            console.error(`novadax-client-http: error on getting ticker for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     * Method to get order book on NovaDax from desired symbol
     * @param {string} base
     * @param {string} quote
     * @param {number} limit
     */
    async orderBook(base, quote, limit) {
        const symbol = `${base}_${quote}`;
        console.info(`novadax-client-http: getting order book for symbol ${symbol}`);

        try {
            const res = await this._publicQuery("market/depth", {
                symbol: `${symbol}`,
                limit: limit
            });
            return res.data;
        }
        catch (e) {
            console.error(`novadax-client-http: error on getting order book for symbol ${symbol}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${API_VERSION}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), NovaDaxClientHttp);

module.exports.NovaDaxClientHttp = NovaDaxClientHttp;
