const inversify = require("inversify");

/**
 * @class OrderBook
 */
class OrderBook {
    constructor() {
        this.exchange = null;
        this.asks = [];
        this.bids = [];
    }

    /**
     *
     * @param {string} exchange
     * @return {OrderBook}
     */
    setExchange(exchange) {
        this.exchange = exchange;
        return this;
    }

    /**
     *
     * @param {PriceVolume} ask
     * @return {OrderBook}
     */
    addAsk(ask) {
        this.asks.push(ask);
        return this;
    }

    /**
     *
     * @param {PriceVolume} bid
     * @return {OrderBook}
     */
    addBid(bid) {
        this.bids.push(bid);
        return this;
    }

    /**
     *
     * @param {Date} timestamp
     * @return {OrderBook}
     */
    setTimestamp(timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}

/**
 * @class PriceVolume
 */
class PriceVolume {
    /**
     *
     * @param {number} price
     * @param {number} volume
     */
    constructor(price, volume) {
        this.price = +price;
        this.volume = +volume;
    }
}

/**
 * @class Ticker
 */
class Ticker {
    constructor() {
        this.exchange = null;
        this.symbol = null;
        this.ask = null;
        this.bid = null;
        this.last = null;
        this.high = null;
        this.low = null;
        this.volume = null;
        this.timestamp = null;
    }

    /**
     *
     * @param {string} exchange
     * @return {Ticker}
     */
    setExchange(exchange) {
        this.exchange = exchange;
        return this;
    }

    /**
     *
     * @param {string} symbol
     * @return {Ticker}
     */
    setSymbol(symbol) {
        this.symbol = symbol;
        return this;
    }

    /**
     *
     * @param {number} ask
     * @return {Ticker}
     */
    setAsk(ask) {
        this.ask = +ask;
        return this;
    }

    /**
     *
     * @param {number} bid
     * @return {Ticker}
     */
    setBid(bid) {
        this.bid = +bid;
        return this;
    }

    /**
     *
     * @param {number} last
     * @return {Ticker}
     */
    setLast(last) {
        this.last = +last;
        return this;
    }

    /**
     *
     * @param {number} high
     * @return {Ticker}
     */
    setHigh(high) {
        this.high = +high;
        return this;
    }

    /**
     *
     * @param {number} low
     * @return {Ticker}
     */
    setLow(low) {
        this.low = +low;
        return this;
    }

    /**
     *
     * @param {number} volume
     * @return {Ticker}
     */
    setVolume(volume) {
        this.volume = +volume;
        return this;
    }

    /**
     *
     * @param {Date} timestamp
     * @return {Ticker}
     */
    setTimestamp(timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}

/**
 * @class Index
 */
class Index {
    constructor() {
        this.exchange = null;
        this.symbol = null;
        this.price = null;
    }

    /**
     *
     * @param {string} exchange
     * @return {Index}
     */
    setExchange(exchange) {
        this.exchange = exchange;
        return this;
    }

    /**
     *
     * @param {string} symbol
     * @return {Index}
     */
    setSymbol(symbol) {
        this.symbol = symbol;
        return this;
    }

    /**
     *
     * @param {number} price
     * @return {Index}
     */
    setPrice(price) {
        this.price = +price;
        return this;
    }

    /**
     *
     * @param timestamp
     * @return {Index}
     */
    setTimestamp(timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}

class Candlestick {
    constructor() {
        this.exchange = null;
        this.symbol = null;
        this.open_time = null;
        this.open = null;
        this.high = null;
        this.low = null;
        this.close = null;
        this.volume = null;
        this.quote_volume = null;
        this.trades = null;
        this.close_time = null;
    }

    /**
     *
     * @param {string} exchange
     * @return {Candlestick}
     */
    setExchange(exchange) {
        this.exchange = exchange;
        return this;
    }

    /**
     *
     * @param {string} symbol
     * @return {Candlestick}
     */
    setSymbol(symbol) {
        this.symbol = symbol;
        return this;
    }

    /**
     *
     * @param {number} openTime
     * @return {Candlestick}
     */
    setOpenTime(openTime) {
        this.open_time = +openTime;
        return this;
    }

    /**
     *
     * @param {number} open
     * @return {Candlestick}
     */
    setOpen(open) {
        this.open = +open;
        return this;
    }

    /**
     *
     * @param {number} high
     * @return {Candlestick}
     */
    setHigh(high) {
        this.high = +high;
        return this;
    }

    /**
     *
     * @param {number} low
     * @return {Candlestick}
     */
    setLow(low) {
        this.low = +low;
        return this;
    }

    /**
     *
     * @param {number} close
     * @return {Candlestick}
     */
    setClose(close) {
        this.close = +close;
        return this;
    }

    /**
     *
     * @param {number} volume
     * @return {Candlestick}
     */
    setVolume(volume) {
        this.volume = +volume;
        return this;
    }

    /**
     *
     * @param {number} quoteVolume
     * @return {Candlestick}
     */
    setQuoteVolume(quoteVolume) {
        this.quote_volume = +quoteVolume;
        return this;
    }

    /**
     *
     * @param {number} closeTime
     * @return {Candlestick}
     */
    setCloseTime(closeTime) {
        this.close_time = +closeTime;
        return this;
    }
}

class FutureContract {
    constructor() {
        this.exchange = null;
        this.symbol = null;
        this.description = null;
        this.expiration_date = null;
        this.expired = null;
        this.active = null;
    }

    /**
     *
     * @param {string} exchange
     * @return {FutureContract}
     */
    setExchange(exchange) {
        this.exchange = exchange;
        return this;
    }

    /**
     *
     * @param {string} symbol
     * @return {FutureContract}
     */
    setSymbol(symbol) {
        this.symbol = symbol;
        return this;
    }

    /**
     *
     * @param {string} description
     * @return {FutureContract}
     */
    setDescription(description) {
        this.description = description;
        return this;
    }

    /**
     *
     * @param {string} expirationDate
     * @return {FutureContract}
     */
    setExpirationDate(expirationDate) {
        this.expiration_date = expirationDate;
        return this;
    }

    /**
     *
     * @param {string} expired
     * @return {FutureContract}
     */
    setExpired(expired) {
        this.expired = expired;
        return this;
    }

    /**
     *
     * @param {string} active
     * @return {FutureContract}
     */
    setActive(active) {
        this.active = active;
        return this;
    }
}

/**
 * @class OrderBookFactory
 */
class OrderBookFactory {
    /**
     *
     * @param {string} exchange
     * @param {PriceVolume[]} asks
     * @param {PriceVolume[]} bids
     * @param {Date} timestamp
     * @return {OrderBook}
     */
    create(exchange, asks, bids, timestamp) {
        const orderBook = new OrderBook();
        orderBook.setExchange(exchange)
            .setTimestamp(timestamp)

        for(let i=0; i < asks.length; i++) {
            const ask = asks[i];
            const bid = bids[i];

            if (typeof bids[i] === "undefined") {
                continue;
            }

            orderBook.addAsk(ask);
            orderBook.addBid(bid);
        }

        return orderBook;
    }
}

/**
 * @class TickerFactory
 */
class TickerFactory {
    /**
     *
     * @param {string} exchange
     * @param {string} symbol
     * @param {number} ask
     * @param {number} bid
     * @param {number} last
     * @param {number} high
     * @param {number} low
     * @param {number} vol
     * @param {Date} timestamp
     * @return {Ticker}
     */
    create(exchange, symbol, ask, bid, last, high, low, vol, timestamp) {
        const ticker = new Ticker();
        ticker.setExchange(exchange)
            .setSymbol(symbol)
            .setAsk(ask)
            .setBid(bid)
            .setLast(last)
            .setHigh(high)
            .setLow(low)
            .setVolume(vol)
            .setTimestamp(timestamp)

        return ticker;
    }
}

/**
 * @class IndexFactory
 */
class IndexFactory {
    /**
     *
     * @param {string} exchange
     * @param {string} symbol
     * @param {number} price
     * @param {Date} timestamp
     * @return {Index}
     */
    create(exchange, symbol, price, timestamp) {
        return (new Index())
            .setExchange(exchange)
            .setPrice(price)
            .setSymbol(symbol)
            .setTimestamp(new Date(timestamp))
    }
}

/**
 * @class CandlestickFactory
 */
class CandlestickFactory {
    /**
     *
     * @param {string} exchange
     * @param {string} symbol
     * @param {number} openTime
     * @param {number} open
     * @param {number} high
     * @param {number} low
     * @param {number} close
     * @param {number} closeTime
     * @param {number} volume
     * @param {number} volumeQuote
     * @return {Candlestick}
     */
    create(exchange, symbol, openTime, open, high, low, close, closeTime, volume, volumeQuote) {
        return (new Candlestick())
            .setExchange(exchange)
            .setSymbol(symbol)
            .setOpenTime(openTime)
            .setOpen(open)
            .setHigh(high)
            .setLow(low)
            .setClose(close)
            .setCloseTime(closeTime)
            .setVolume(volume)
            .setQuoteVolume(volumeQuote)
    }
}

/**
 * @class FutureContractFactory
 */
class FutureContractFactory {
    /**
     *
     * @param {string} exchange
     * @param symbol
     * @param expirationDate
     * @param description
     * @param expired
     * @param active
     */
    create(exchange, symbol, expirationDate, description, expired, active) {
        return (new FutureContract())
            .setExchange(exchange)
            .setSymbol(symbol)
            .setDescription(description)
            .setExpirationDate(expirationDate)
            .setExpired(expired)
            .setActive(active);
    }
}

inversify.decorate(inversify.injectable(), TickerFactory);
inversify.decorate(inversify.injectable(), IndexFactory);
inversify.decorate(inversify.injectable(), OrderBookFactory);
inversify.decorate(inversify.injectable(), FutureContractFactory);

module.exports.Candlestick = Candlestick;
module.exports.FutureContractFactory = FutureContractFactory;
module.exports.Index = Index;
module.exports.OrderBook = OrderBook;
module.exports.PriceVolume = PriceVolume;
module.exports.Ticker = Ticker;
module.exports.CandlestickFactory = CandlestickFactory;
module.exports.IndexFactory = IndexFactory;
module.exports.OrderBookFactory = OrderBookFactory;
module.exports.TickerFactory = TickerFactory;
