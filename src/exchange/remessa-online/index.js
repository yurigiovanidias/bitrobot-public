module.exports.RemessaOnlineClient = require("./remessa-online-client-http").RemessaOnlineClientHttp;
module.exports.RemessaOnlineService = require("./remessa-online-service").RemessaOnlineService;
module.exports.RemessaOnlineSpecificService = require("./remessa-online-specific-service").RemessaOnlineSpecificService;
