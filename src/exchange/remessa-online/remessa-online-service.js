const inversify = require("inversify");
const { TYPES } = require("../../utils");
const CONSTANT = require("./constant");
const { Index } = require("../../exchange/type");

class RemessaOnlineService {
    /**
     *
     * @param {RemessaOnlineSpecificService} remessaOnlineSpecificService
     */
    constructor(remessaOnlineSpecificService) {
        this.remessaOnlineSpecificService = remessaOnlineSpecificService;
    }

    /**
     *
     * @param {string} symbol
     * @return {Promise<Index>}
     */
    async indexSymbol(symbol) {
        const [base, quote] = symbol.split("_");
        let auxBase = base;
        let auxQuote = quote;

        if (auxBase === "USDT") {
            auxBase = "USD"
        }

        if (auxQuote === "USDT") {
            auxQuote = "USD"
        }

        const auxSymbol = `${auxBase}_${auxQuote}`;

        if (auxSymbol !== "BRL_USD" && auxSymbol !== "USD_BRL") {
            throw new Error(`Symbol ${auxSymbol} not allowed`);
        }

        const result = await this.remessaOnlineSpecificService.quote(auxBase);

        return (new Index())
            .setExchange(CONSTANT.ACRONYM)
            .setSymbol(symbol)
            .setPrice(result.value)
            .setTimestamp(new Date);
    }
}

inversify.decorate(inversify.injectable(), RemessaOnlineService);
inversify.decorate(inversify.inject(TYPES.RemessaOnlineSpecificService), RemessaOnlineService, 0);

module.exports.RemessaOnlineService = RemessaOnlineService
