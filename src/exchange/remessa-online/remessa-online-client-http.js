const inversify = require("inversify");
const { AbstractExchangeClient } = require("../abstract-exchange-client");

const URL = "https://www.remessaonline.com.br/api";

class RemessaOnlineClientHttp extends AbstractExchangeClient {
    /**
     * Method to get specific quote (by code) on RemessaOnline
     * @param {string} currency
     */
    async quote(currency) {
        console.info(`remessa-online-api-client-http: getting symbol for ${currency}`);

        try {
            const res = await this._publicQuery(`current-quotation/${currency}/COM`, null);
            return res;
        }
        catch (e) {
            console.error(`remessa-online-api-client-http: error on getting symbol for ${currency}`);
            throw e;
        }
    }

    /**
     *
     * @param {string} uri
     * @return {string}
     * @private
     */
    _getURL(uri) {
        return `${URL}/${uri}`;
    }
}

inversify.decorate(inversify.injectable(), RemessaOnlineClientHttp);

module.exports.RemessaOnlineClientHttp = RemessaOnlineClientHttp;
