const inversify = require("inversify");
const { TYPES } = require("../../utils");

class RemessaOnlineSpecificService {
    /**
     *
     * @param {RemessaOnlineClientHttp} remessaOnlineClient
     */
    constructor(remessaOnlineClient) {
        this._client = remessaOnlineClient;
    }

    /**
     *
     * @param {string} currency
     */
    async quote(currency) {
        return this._client.quote(currency);
    }
}

inversify.decorate(inversify.injectable(), RemessaOnlineSpecificService);
inversify.decorate(inversify.inject(TYPES.RemessaOnlineClient), RemessaOnlineSpecificService, 0);

module.exports.RemessaOnlineSpecificService = RemessaOnlineSpecificService;
