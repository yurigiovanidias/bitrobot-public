const inversify = require('inversify');
const request = require('superagent');
const DEFAULT_TIMEOUT_GET = 3000;

/**
 * Classe that encapsule all basic methods to exchange clients
 * @abstract
 */
class AbstractExchangeClient {
    /**
     *
     * @param {string} uri
     * @param {object} params
     * @protected
     */
    async _publicQuery (uri, params) {
        return request('GET', this._getURL(uri))
            .query(params)
            .set('Content-Type', 'application/json')
            /**
             * @todo Remove User-Agent
             */
            .set('User-Agent', 'node-superagent/5.0.2')
            // .timeout(DEFAULT_TIMEOUT_GET)
            .then((res) => {
                return res.body;
            });
    };

    /**
     *
     * @param {string} uri
     * @protected
     */
    _getURL(uri) {
        throw new Error('Method {_getURL} must be implemented');
    }
}

inversify.decorate(inversify.injectable(), AbstractExchangeClient);

module.exports.AbstractExchangeClient = AbstractExchangeClient;
