const { CONSTANT, TYPES } = require("../utils");
const { ExchangeManager } = require("../exchange/manager");
const { ExchangeManagerWriter } = require("../exchange/exchange-manager-writer");
const { ExchangeServiceRegistry } = require("../exchange/service-registry");
const { IndexSymbolRepository, OrderBookRepository, TickerRepository } = require("../repository");
const { IndexFactory, OrderBookFactory, TickerFactory, FutureContractFactory} = require("../exchange/type");
const { BitrobotService } = require("../exchange/bitrobot");
const { AwesomeApiClient, AwesomeApiSpecificService, AwesomeApiService } = require("../exchange/awesomeapi");
const { B2UClient, B2USpecificService, B2UService } = require("../exchange/b2u");
const { BinanceClient, BinanceSpecificService, BinanceService } = require("../exchange/binance");
const { BitcambioClient, BitcambioSpecificService, BitcambioService } = require("../exchange/bitcambio");
const { BitcointradeClient, BitcointradeSpecificService, BitcointradeService } = require("../exchange/bitcointrade");
const { BithumbClient, BithumbSpecificService, BithumbService } = require("../exchange/bithumb");
const { BitprecoClient, BitprecoSpecificService, BitprecoService } = require("../exchange/bitpreco");
const { BitRecifeClient, BitRecifeSpecificService, BitRecifeService } = require("../exchange/bitrecife");
const { BitmexClient, BitmexSpecificService, BitmexService } = require("../exchange/bitmex");
const { EMXClient, EMXService, EMXSpecificService } = require("../exchange/emx");
const { FTXClient, FTXService, FTXSpecificService } = require("../exchange/ftx");
const { KucoinClient, KucoinSpecificService, KucoinService } = require("../exchange/kucoin");
const { MercadoBitcoinClient, MercadoBitcoinSpecificService, MercadoBitcoinService } = require("../exchange/mercadobitcoin");
const { NovaDaxClient, NovaDaxSpecificService, NovaDaxService } = require("../exchange/novadax");
const { OSLClient, OSLSpecificService, OSLService } = require("../exchange/osl");
const { PoloniexClient, PoloniexSpecificService, PoloniexService } = require("../exchange/poloniex");
const { RemessaOnlineClient, RemessaOnlineService, RemessaOnlineSpecificService } = require("../exchange/remessa-online");
const dataSourceMongo = require('../../config/data-source');

class ServiceProvider {
    static async register(container) {
        await registerDataSource(container);
        registerFactories(container);
        registerRepositories(container);
        registerClients(container);
        registerServices(container);
        registerManagers(container);
    }
}

const registerDataSource = async (container) => {
    const mongoClient = await dataSourceMongo.connect();

    const [tickerCollection, orderBookCollection, indexesCollection] = await Promise.all([
        mongoClient.getCollection("tickers"),
        mongoClient.getCollection("order_books"),
        mongoClient.getCollection("indexes"),
    ]);

    container.bind(TYPES.TickersCollection).toConstantValue(tickerCollection);
    container.bind(TYPES.OrderBooksCollection).toConstantValue(orderBookCollection);
    container.bind(TYPES.IndexesCollection).toConstantValue(indexesCollection);
};

const registerRepositories = (container) => {
    container.bind(TYPES.TickerRepository).to(TickerRepository);
    container.bind(TYPES.IndexSymbolRepository).to(IndexSymbolRepository);
    container.bind(TYPES.OrderBookRepository).to(OrderBookRepository);
};

/**
 * Register clients
 */
const registerClients = (container) => {
    container.bind(TYPES.AwesomeApiClient).to(AwesomeApiClient);
    container.bind(TYPES.B2UClient).to(B2UClient);
    container.bind(TYPES.BinanceClient).to(BinanceClient);
    container.bind(TYPES.BitcambioClient).to(BitcambioClient);
    container.bind(TYPES.BitcointradeClient).to(BitcointradeClient);
    container.bind(TYPES.BithumbClient).to(BithumbClient);
    container.bind(TYPES.BitprecoClient).to(BitprecoClient);
    container.bind(TYPES.BitRecifeClient).to(BitRecifeClient);
    container.bind(TYPES.BitmexClient).to(BitmexClient);
    container.bind(TYPES.EMXClient).to(EMXClient);
    container.bind(TYPES.FTXClient).to(FTXClient);
    container.bind(TYPES.KucoinClient).to(KucoinClient);
    container.bind(TYPES.MercadoBitcoinClient).to(MercadoBitcoinClient);
    container.bind(TYPES.NovaDaxClient).to(NovaDaxClient);
    container.bind(TYPES.OSLClient).to(OSLClient);
    container.bind(TYPES.PoloniexClient).to(PoloniexClient);
    container.bind(TYPES.RemessaOnlineClient).to(RemessaOnlineClient);
};

/**
 * Register services
 */
const registerServices = (container) => {
    container.bind(TYPES.BitRobotService).to(BitrobotService);

    container.bind(TYPES.AwesomeApiSpecificService).to(AwesomeApiSpecificService);
    container.bind(TYPES.AwesomeApiService).to(AwesomeApiService);

    container.bind(TYPES.B2USpecificService).to(B2USpecificService);
    container.bind(TYPES.B2UService).to(B2UService);

    container.bind(TYPES.BinanceSpecificService).to(BinanceSpecificService);
    container.bind(TYPES.BinanceService).to(BinanceService);

    container.bind(TYPES.BitcambioSpecificService).to(BitcambioSpecificService);
    container.bind(TYPES.BitcambioService).to(BitcambioService);

    container.bind(TYPES.BitcointradeSpecificService).to(BitcointradeSpecificService);
    container.bind(TYPES.BitcointradeService).to(BitcointradeService);

    container.bind(TYPES.BithumbSpecificService).to(BithumbSpecificService);
    container.bind(TYPES.BithumbService).to(BithumbService);

    container.bind(TYPES.BitprecoSpecificService).to(BitprecoSpecificService);
    container.bind(TYPES.BitprecoService).to(BitprecoService);

    container.bind(TYPES.BitRecifeSpecificService).to(BitRecifeSpecificService);
    container.bind(TYPES.BitRecifeService).to(BitRecifeService);

    container.bind(TYPES.BitmexSpecificService).to(BitmexSpecificService);
    container.bind(TYPES.BitmexService).to(BitmexService);

    container.bind(TYPES.EMXSpecificService).to(EMXSpecificService);
    container.bind(TYPES.EMXService).to(EMXService);

    container.bind(TYPES.FTXSpecificService).to(FTXSpecificService);
    container.bind(TYPES.FTXService).to(FTXService);

    container.bind(TYPES.KucoinSpecificService).to(KucoinSpecificService);
    container.bind(TYPES.KucoinService).to(KucoinService);

    container.bind(TYPES.MercadoBitcoinSpecificService).to(MercadoBitcoinSpecificService);
    container.bind(TYPES.MercadoBitcoinService).to(MercadoBitcoinService);

    container.bind(TYPES.NovaDaxSpecificService).to(NovaDaxSpecificService);
    container.bind(TYPES.NovaDaxService).to(NovaDaxService);

    container.bind(TYPES.OSLSpecificService).to(OSLSpecificService);
    container.bind(TYPES.OSLService).to(OSLService);

    container.bind(TYPES.PoloniexSpecificService).to(PoloniexSpecificService);
    container.bind(TYPES.PoloniexService).to(PoloniexService);

    container.bind(TYPES.RemessaOnlineSpecificService).to(RemessaOnlineSpecificService);
    container.bind(TYPES.RemessaOnlineService).to(RemessaOnlineService);

    const exchangeServiceRegistry = getExchangeServiceRegistry(container);

    container.bind(TYPES.ExchangeServiceRegistry).toConstantValue(exchangeServiceRegistry);
};

const getExchangeServiceRegistry = (container) => {
    const exchangeServiceRegistry = new ExchangeServiceRegistry();
    const bitrobotService = container.get(TYPES.BitRobotService);
    const awesomeApiService = container.get(TYPES.AwesomeApiService);
    const b2uService = container.get(TYPES.B2UService);
    const binanceService = container.get(TYPES.BinanceService);
    const bitcambioService = container.get(TYPES.BitcambioService);
    const bitcointradeService = container.get(TYPES.BitcointradeService);
    const bithumbService = container.get(TYPES.BithumbService);
    const bitprecoService = container.get(TYPES.BitprecoService);
    const bitrecifeService = container.get(TYPES.BitRecifeService);
    const bitmexService = container.get(TYPES.BitmexService);
    const emxService = container.get(TYPES.EMXService);
    const ftxService = container.get(TYPES.FTXService);
    const kucoinService = container.get(TYPES.KucoinService);
    const mercadobitcoinService = container.get(TYPES.MercadoBitcoinService);
    const novadaxService = container.get(TYPES.NovaDaxService);
    const oslService = container.get(TYPES.OSLService);
    const poloniexService = container.get(TYPES.PoloniexService);
    const remessaOnlineService = container.get(TYPES.RemessaOnlineService);

    exchangeServiceRegistry.add(CONSTANT.BITROBOT.ACRONYM, bitrobotService);
    exchangeServiceRegistry.add(CONSTANT.AWESOMEAPI.ACRONYM, awesomeApiService);
    exchangeServiceRegistry.add(CONSTANT.B2U.ACRONYM, b2uService);
    exchangeServiceRegistry.add(CONSTANT.BINANCE.ACRONYM, binanceService);
    exchangeServiceRegistry.add(CONSTANT.BITMEX.ACRONYM, bitmexService);
    exchangeServiceRegistry.add(CONSTANT.BITCAMBIO.ACRONYM, bitcambioService);
    exchangeServiceRegistry.add(CONSTANT.BITCOINTRADE.ACRONYM, bitcointradeService);
    exchangeServiceRegistry.add(CONSTANT.BITHUMB.ACRONYM, bithumbService);
    exchangeServiceRegistry.add(CONSTANT.BITPRECO.ACRONYM, bitprecoService);
    exchangeServiceRegistry.add(CONSTANT.BITRECIFE.ACRONYM, bitrecifeService);
    exchangeServiceRegistry.add(CONSTANT.EMX.ACRONYM, emxService);
    exchangeServiceRegistry.add(CONSTANT.FTX.ACRONYM, ftxService);
    exchangeServiceRegistry.add(CONSTANT.KUCOIN.ACRONYM, kucoinService);
    exchangeServiceRegistry.add(CONSTANT.MERCADOBITCOIN.ACRONYM, mercadobitcoinService);
    exchangeServiceRegistry.add(CONSTANT.NOVADAX.ACRONYM, novadaxService);
    exchangeServiceRegistry.add(CONSTANT.OSL.ACRONYM, oslService);
    exchangeServiceRegistry.add(CONSTANT.POLONIEX.ACRONYM, poloniexService);
    exchangeServiceRegistry.add(CONSTANT.REMESSAONLINE.ACRONYM, remessaOnlineService);

    return exchangeServiceRegistry;
}

/**
 * Register Managers
 */
const registerManagers = (container) => {
    container.bind(TYPES.ExchangeManager).to(ExchangeManager);
    container.bind(TYPES.ExchangeManagerWriter).to(ExchangeManagerWriter);
};

/**
 * Register Factories
 */
const registerFactories = (container) => {
    container.bind(TYPES.IndexFactory).to(IndexFactory);
    container.bind(TYPES.OrderBookFactory).to(OrderBookFactory);
    container.bind(TYPES.TickerFactory).to(TickerFactory);
    container.bind(TYPES.FutureContractFactory).to(FutureContractFactory);
};

/**
 * Register Managers
 */
const registerCollections = (container) => {};

/**
 * Register Managers
 */
const registerPresenters = (container) => {};

module.exports.ServiceProvider = ServiceProvider;
