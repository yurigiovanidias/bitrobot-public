const inversify = require('inversify');
const { TYPES } = require('../utils');

class OrderBookRepository {
    constructor(collection) {
        this._collection = collection;
    }

    /**
     * Method to insert an OrderBook list
     * @param {OrderBook[]} orderBooks
     * @returns {Promise<void>}
     */
    async insertMany(orderBooks) {
        return this._collection.insertMany(orderBooks);
    }
}

inversify.decorate(inversify.injectable(), OrderBookRepository);
inversify.decorate(inversify.inject(TYPES.OrderBooksCollection), OrderBookRepository, 0);

module.exports.OrderBookRepository = OrderBookRepository;
