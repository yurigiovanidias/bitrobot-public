const inversify = require('inversify');
const { TYPES } = require('../utils');

class TickerRepository {
    constructor(collection, tickerFactory) {
        this._collection = collection;
        this._tickerFactory = tickerFactory;
    }

    /**
     * Method to find all tickers
     * @returns {Promise<*>}
     */
    async findAll() {
        return this._collection.find({}).toArray();
    }

    /**
     * Method to find ticker by id
     * @param id
     * @returns {Promise<*>}
     */
    async findById(id) {
        return this._collection.findOne({
            id: id,
        });
    }

    /**
     * Method to get IndexSymbol by exchanges and symbol
     * @param {string[]} exchanges
     * @param {string} symbol
     * @param {number} minutesDiff
     * @returns {Index[]}
     */
    async getLatestByExchangesAndSymbol(exchanges, symbol, minutesDiff = 1) {
        const timestamp = new Date();
        timestamp.setMinutes(timestamp.getMinutes() - minutesDiff);

        const result = await this._collection.find({
            exchange: {
                $in: exchanges
            },
            timestamp: {
                $gt: timestamp,
            },
            symbol: symbol,
        }).toArray();

        return result.map(row => {
            return this._tickerFactory.create(row.exchange, row.symbol, row.ask, row.bid, row.last, row.high, row.low, row.timestamp);
        });
    }

    /**
     * Method to remove documents by exchange and symbol
     * @param exchanges
     * @param symbols
     * @return {Promise<void>}
     */
    async removeByExchangeAndSymbol(exchanges, symbols) {
        return this._collection.deleteMany({
            exchange: {
                $in: exchanges
            },
            symbol: {
                $in: symbols
            },
        });
    }

    /**
     * Method to insert a Ticker
     * @param {Ticker[]} tickers
     * @returns {Promise<void>}
     */
    async insertMany(tickers) {
        const promises = tickers.map(ticker => {
            return this._collection.replaceOne({
                exchange: ticker.exchange,
                symbol: ticker.symbol,
            }, ticker, {
                upsert: true,
            });
        });

        try {
            return Promise.all(promises);
        }
        catch (e) {
            console.log("Some error while executing replaceOne", e.message);
        }
    }
}

inversify.decorate(inversify.injectable(), TickerRepository);
inversify.decorate(inversify.inject(TYPES.TickersCollection), TickerRepository, 0);
inversify.decorate(inversify.inject(TYPES.TickerFactory), TickerRepository, 1);

module.exports.TickerRepository = TickerRepository;
