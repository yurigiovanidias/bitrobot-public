const inversify = require('inversify');
const { TYPES } = require('../utils');

class IndexSymbolRepository {
    constructor(collection, indexFactory) {
        this._collection = collection;
        this._indexFactory = indexFactory;
    }

    /**
     * Method to return all IndexSymbol
     * @return {Promise<T[]>}
     */
    async getAll() {
        return this._collection.find({}).toArray();
    }

    /**
     * Method to get IndexSymbol by exchanges and symbol
     * @param {string[]} exchanges
     * @param {string} symbol
     * @param {number} minutesDiff
     * @returns {Index[]}
     */
    async getLatestByExchangesAndSymbol(exchanges, symbol, minutesDiff = 1) {
        const timestamp = new Date();

        timestamp.setMinutes(timestamp.getMinutes() - minutesDiff);

        const result = await this._collection.find({
            exchange: {
                $in: exchanges
            },
            timestamp: {
                $gt: timestamp,
            },
            symbol: symbol,
        }).toArray();

        return result.map(row => {
            return this._indexFactory.create(row.exchange, row.symbol, row.price, row.timestamp);
        });
    }

    /**
     * Method to remove documents by exchange and symbol
     * @param exchanges
     * @param symbols
     * @return {Promise<void>}
     */
    async removeByExchangeAndSymbol(exchanges, symbols) {
        return this._collection.deleteMany({
            exchange: {
                $in: exchanges
            },
            symbol: {
                $in: symbols
            },
        });
    }

    /**
     * Method to insert an IndexSymbol list
     * @param {Index[]} indexesSymbol
     * @returns {Promise<void>}
     */
    async insertMany(indexesSymbol) {
        const promises = indexesSymbol.map(index => {
            return this._collection.replaceOne({
                exchange: index.exchange,
                symbol: index.symbol,
            }, index, {
                upsert: true,
            });
        });

        try {
            return Promise.all(promises);
        }
        catch (e) {
            console.log("Some error while executing replaceOne", e.message);
        }
    }
}

inversify.decorate(inversify.injectable(), IndexSymbolRepository);
inversify.decorate(inversify.inject(TYPES.IndexesCollection), IndexSymbolRepository, 0);
inversify.decorate(inversify.inject(TYPES.IndexFactory), IndexSymbolRepository, 1);

module.exports.IndexSymbolRepository = IndexSymbolRepository;
