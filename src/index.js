const inversify = require("inversify");
require("reflect-metadata");

const container = new inversify.Container();
const { TYPES } = require("./utils");
const { ServiceProvider } = require("./service-provider/service-provider");
const { index } = require("../handler/ticker");
// const { index } = require("../handler/index-symbol");
// const { index } = require("../handler/orderbook");

(async () => {
    await ServiceProvider.register(container);

    /**
     * @type ExchangeManager
     */
    const exchangeManager = container.get(TYPES.ExchangeManager);
    /**
     * @type ExchangeManagerWriter
     */
    const exchangeManagerWriter = container.get(TYPES.ExchangeManagerWriter);

    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

    await index({
        pathParameters: {
            symbol: "BTC_BRL"
        }
    }, {});

    // const result = await exchangeManager.indexSymbolRead("BTC_BRL", [
    //     CONSTANT.BINANCE.ACRONYM,
    //     CONSTANT.BITROBOT.ACRONYM,
    //     CONSTANT.BITMEX.ACRONYM,
    // ]);
    //
    // await exchangeManagerWriter.indexSymbolWrite(result);

    // const result = await exchangeManager.orderBookRead("BTC_BRL", [
    //     CONSTANT.BITCAMBIO.ACRONYM,
    //     CONSTANT.BITCOINTRADE.ACRONYM,
    //     CONSTANT.BITPRECO.ACRONYM,
    //     CONSTANT.BITRECIFE.ACRONYM,
    //     CONSTANT.BITMEX.ACRONYM,
    //     CONSTANT.MERCADOBITCOIN.ACRONYM,
    //     CONSTANT.NOVADAX.ACRONYM,
    // ]);
    //
    // await exchangeManagerWriter.orderBookWrite(result);

    // const result = await exchangeManager.tickerRead("BTC_BRL", [
    //     CONSTANT.BITCAMBIO.ACRONYM,
    //     CONSTANT.BITCOINTRADE.ACRONYM,
    //     CONSTANT.BITPRECO.ACRONYM,
    //     CONSTANT.BITRECIFE.ACRONYM,
    //     CONSTANT.BITMEX.ACRONYM,
    //     CONSTANT.MERCADOBITCOIN.ACRONYM,
    //     CONSTANT.NOVADAX.ACRONYM,
    // ]);

    // await exchangeManagerWriter.tickerWrite(result);

    // console.log(result);
})();
