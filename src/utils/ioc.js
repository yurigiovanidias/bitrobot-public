module.exports = {
    // AwesomeApi
    AwesomeApiClient: 'AwesomeApiClient',
    AwesomeApiService: 'AwesomeApiService',
    AwesomeApiSpecificService: 'AwesomeApiSpecificService',

    // BITROBOT
    BitRobotService: 'BitRobotService',

    // B2U
    B2UClient: 'B2UClient',
    B2UService: 'B2UService',
    B2USpecificService: 'B2USpecificService',

    // BINANCE
    BinanceClient: 'BinanceClient',
    BinanceService: 'BinanceService',
    BinanceSpecificService: 'BinanceSpecificService',

    // BITCAMBIO
    BitcambioClient: 'BitcambioClient',
    BitcambioService: 'BitcambioService',
    BitcambioSpecificService: 'BitcambioSpecificService',

    // BITCOINTRADE
    BitcointradeClient: 'BitcointradeClient',
    BitcointradeService: 'BitcointradeService',
    BitcointradeSpecificService: 'BitcointradeSpecificService',

    // BITHUMB
    BithumbClient: 'BithumbClient',
    BithumbService: 'BithumbService',
    BithumbSpecificService: 'BithumbSpecificService',

    // BITPRECO
    BitprecoClient: 'BitprecoClient',
    BitprecoService: 'BitprecoService',
    BitprecoSpecificService: 'BitprecoSpecificService',

    // BITRECIFE
    BitRecifeClient: 'BitRecifeClient',
    BitRecifeService: 'BitRecifeService',
    BitRecifeSpecificService: 'BitRecifeSpecificService',

    // BITMEX
    BitmexClient: 'BitmexClient',
    BitmexService: 'BitmexService',
    BitmexSpecificService: 'BitmexSpecificService',

    // EMX
    EMXClient: 'EMXClient',
    EMXSpecificService: 'EMXSpecificService',
    EMXService: 'EMXService',

    // FTX
    FTXClient: 'FTXClient',
    FTXSpecificService: 'FTXSpecificService',
    FTXService: 'FTXService',

    // MERCADOBITCOIN
    MercadoBitcoinClient: 'MercadoBitcoinClient',
    MercadoBitcoinService: 'MercadoBitcoinService',
    MercadoBitcoinSpecificService: 'MercadoBitcoinSpecificService',

    // KUCOIN
    KucoinClient: 'KucoinClient',
    KucoinService: 'KucoinService',
    KucoinSpecificService: 'KucoinSpecificService',

    // NOVADAX
    NovaDaxClient: 'NovaDaxClient',
    NovaDaxService: 'NovaDaxService',
    NovaDaxSpecificService: 'NovaDaxSpecificService',

    // POLONIEX
    PoloniexClient: 'PoloniexClient',
    PoloniexService: 'PoloniexService',
    PoloniexSpecificService: 'PoloniexSpecificService',

    // OSL
    OSLClient: 'OSLClient',
    OSLService: 'OSLService',
    OSLSpecificService: 'OSLSpecificService',

    // RemessaOnline
    RemessaOnlineClient: 'RemessaOnlineClient',
    RemessaOnlineService: 'RemessaOnlineService',
    RemessaOnlineSpecificService: 'RemessaOnlineSpecificService',

    // DOMAIN
    ExchangeServiceRegistry: 'ExchangeServiceRegistry',
    ExchangeManager: 'ExchangeManager',
    ExchangeManagerWriter: 'ExchangeManagerWriter',
    IndexFactory: 'IndexFactory',
    OrderBookFactory: 'OrderBookFactory',
    TickerFactory: 'TickerFactory',
    FutureContractFactory: 'FutureContractFactory',

    // Database, Collections
    TickersCollection: 'TickersCollection',
    TickerRepository: 'TickerRepository',
    OrderBooksCollection: 'OrderBooksCollection',
    OrderBookRepository: 'OrderBookRepository',
    IndexesCollection: 'IndexesCollection',
    IndexSymbolRepository: 'IndexSymbolRepository',
};
