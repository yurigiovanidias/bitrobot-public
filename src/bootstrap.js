const inversify = require("inversify");
require('reflect-metadata');

const { ServiceProvider } = require("../src/service-provider/service-provider");
const dataSourceMongo = require('../config/data-source');

let container = null;

module.exports.Start = async () => {
    if (container !== null) {
        // console.log("Using cached container");

        return container;
    }

    container = new inversify.Container();
    await ServiceProvider.register(container);

    return container;
}

module.exports.TearDown = async () => {
    container = null;
    await dataSourceMongo.disconnect();
}
