const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

const handlerTicker = require('../../handler/ticker');
const handlerOrderBook = require('../../handler/orderbook');
const handlerIndexSymbol = require('../../handler/index-symbol');
const handlerCandle = require('../../handler/candle');
const handlerFutureContract = require('../../handler/future-contract');

// ORDERBOOK
app.get('/orderbooks/:symbol', async (req, res) => {
    req.pathParameters = req.params;
    req.queryStringParameters = req.query;
    req.multiValueQueryStringParameters = req.query;

    const result = await handlerOrderBook.index(req, {});

    res.send(result.body);
});

// TICKER
app.get('/tickers/:symbol', async (req, res) => {
    req.pathParameters = req.params;
    req.queryStringParameters = req.query;
    req.multiValueQueryStringParameters = req.query;

    const result = await handlerTicker.index(req, {});

    res.send(result.body);
});

// INDEX SYMBOL
app.get('/index/:symbol', async (req, res) => {
    req.pathParameters = req.params;
    req.queryStringParameters = req.query;

    const result = await handlerIndexSymbol.index(req, {});

    res.send(result.body);
});

// CANDLE
app.get('/candles/:symbol', async (req, res) => {
    req.pathParameters = req.params;
    req.queryStringParameters = req.query;
    req.multiValueQueryStringParameters = req.query;

    const result = await handlerCandle.index(req, {});

    res.send(result.body);
});

// FUTURE CONTRACT
app.get('/future-contracts/:symbol', async (req, res) => {
    req.pathParameters = req.params;
    req.queryStringParameters = req.query;
    req.multiValueQueryStringParameters = req.query;

    const result = await handlerFutureContract.index(req, {});

    res.send(result.body);
});

// SERVER LISTEN
app.listen(9229, () => {
    // console.log("Server running on port 9229!");
});
