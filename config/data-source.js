const MongoClient = require('mongodb').MongoClient;

/**
 *
 * @type {MongoDbClient}
 */
let connect = null;

/**
 * Method to connect in mongo data
 */
module.exports.connect = async () => {
    if (connect != null && connect.ping()) {
        console.info("Use database connection cache");
        return Promise.resolve(connect);
    }

    const uri = "mongodb+srv://robot:mnwZfllEnwbvu7Pe@robot-pfmmi.mongodb.net/robot?retryWrites=true&writeConcern=majority&w=majority"
    const mongoClient = new MongoDbClient(uri, "bitrobot-public");

    await mongoClient.connect(uri, {
        useNewUrlParser: true,
    });

    connect = mongoClient;
    return mongoClient;
};

/**
 * Method to disconnect from database and clear connect variable to avoid cache
 * @return {Promise<void>}
 */
module.exports.disconnect = async () => {
    await connect.disconnect();
    connect = null;
};

class MongoDbClient {
    constructor(url, databaseName) {
        this.url = url;
        this.databaseName = databaseName;
    }

    async getCollection(name) {
        return (await this.getDatabase()).collection(name);
    }

    async getDatabase() {
        if (!this.database) {
            this.database = (await this.connect()).db(this.databaseName);
        }

        return this.database;
    }

    async connect() {
        if (!this.client) {
            this.client = await MongoClient.connect(this.url, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                connectTimeoutMS: 10000,
            });
        }

        return this.client;
    }

    ping() {
        return this.client.isConnected();
    }

    // async ping() {
    //     return (await this.connect()).isConnected();
    // }

    async disconnect() {
        return this.client.close();
    }
}
