const App = require("../src/bootstrap");
const { TYPES } = require("../src/utils");
const mapQueryString = require("amazon-api-gateway-querystring");

/**
 * Method to get ticker from desired Exchange
 */
module.exports.index = async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    const container = await App.Start();
    const pathParameters = event.pathParameters;
    const queryParameters = event.multiValueQueryStringParameters;

    // console.log('ticker: start to get from Exchange', JSON.stringify(queryParameters), pathParameters);

    try {
        let exchanges = [
            "BN", "BTCT", "BPR", "MB",
        ];

        if (queryParameters != null && typeof queryParameters.exchanges !== "undefined") {
            exchanges = queryParameters.exchanges;
        }

        const future = queryParameters.future || false;
        const [base, quote] = pathParameters.symbol.toUpperCase().split("_");
        const symbol = `${base}_${quote}`;
        /**
         *
         * @type ExchangeManager
         */
        const manager = container.get(TYPES.ExchangeManager);
        /**
         * @type ExchangeManagerWriter
         */
        const exchangeManagerWriter = container.get(TYPES.ExchangeManagerWriter);
        let [tickers, newTickers] = await manager.tickerRead(symbol, exchanges, future);

        if (typeof newTickers !== "undefined" && newTickers.length > 0) {
            await exchangeManagerWriter.tickerWrite(newTickers, "http");
            tickers = tickers.concat(newTickers);
        }

        await App.TearDown();

        return {
            statusCode: 200,
            body: JSON.stringify(tickers),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        }
    }
    catch (e) {
        console.log(e);
        return {
            statusCode: 400,
            body: e.message,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
        }
    }
}
