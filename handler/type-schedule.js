const App = require("../src/bootstrap");
const { TYPES } = require("../src/utils")

/**
 * Method to update all types (index, ticker, order book) from database
 */
module.exports.update = async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    // console.log('type-schedule-update: started to update');

    try {
        const container = await App.Start();

        /**
         * @type ExchangeManager
         */
        const exchangeManagerReader = container.get(TYPES.ExchangeManager);
        /**
         * @type ExchangeManagerWriter
         */
        const exchangeManagerWriter = container.get(TYPES.ExchangeManagerWriter);
        /**
         *
         * @type {IndexSymbolRepository}
         */
        const indexSymbolRepository = container.get(TYPES.IndexSymbolRepository);
        /**
         *
         * @type {TickerRepository}
         */
        const tickerRepository = container.get(TYPES.TickerRepository);

        const promises = [];

        promises.push(indexSymbolReader(exchangeManagerReader, exchangeManagerWriter, indexSymbolRepository));
        promises.push(tickerReader(exchangeManagerReader, exchangeManagerWriter, tickerRepository));

        await Promise.all(promises);

        await App.TearDown();
    }
    catch (e) {
        console.log('Some error while executing: type-schedule-update', e.message);
        return {
            statusCode: 400,
            body: e.message,
        }
    }
}

/**
 *
 * @param {ExchangeManager} exchangeManagerReader
 * @param {ExchangeManagerWriter} exchangeManagerWriter
 * @param {IndexSymbolRepository} indexSymbolRepository
 * @return {Promise<void>}
 */
const indexSymbolReader = async (exchangeManagerReader, exchangeManagerWriter, indexSymbolRepository) => {
    const records = await indexSymbolRepository.getAll();
    const aux = {};

    records.forEach(row => {
        if (typeof aux[row.symbol] === "undefined") {
            aux[row.symbol] = [row.exchange];
            return;
        }

        aux[row.symbol].push(row.exchange);
    });

    for (let symbol of Object.keys(aux)) {
        const exchanges = aux[symbol];
        const [indexes, newIndexes] = await exchangeManagerReader.indexSymbolRead(symbol, exchanges, 0);

        if (newIndexes.length > 0) {
            await exchangeManagerWriter.indexSymbolWrite(newIndexes);
        }
     }
};

/**
 *
 * @param {ExchangeManager} exchangeManagerReader
 * @param {ExchangeManagerWriter} exchangeManagerWriter
 * @param {TickerRepository} tickerRepository
 * @return {Promise<void>}
 */
const tickerReader = async (exchangeManagerReader, exchangeManagerWriter, tickerRepository) => {
    const records = await tickerRepository.findAll();
    const aux = {};

    records.forEach(row => {
        if (typeof aux[row.symbol] === "undefined") {
            aux[row.symbol] = [row.exchange];
            return;
        }

        aux[row.symbol].push(row.exchange);
    });

    for (let symbol of Object.keys(aux)) {
        const exchanges = aux[symbol];
        const [tickers, newTickers] = await exchangeManagerReader.tickerRead(symbol, exchanges, 0);

        if (newTickers.length > 0) {
            await exchangeManagerWriter.tickerWrite(newTickers);
        }
    }
};
