const App = require("../src/bootstrap");
const { TYPES } = require("../src/utils");
const mapQueryString = require("amazon-api-gateway-querystring");

/**
 * Method to get ticker from desired Exchange
 */
module.exports.index = async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    const container = await App.Start();
    const pathParameters = event.pathParameters;
    const queryParameters = event.multiValueQueryStringParameters;

    // console.log('future-contract: start to get from Exchange', JSON.stringify(queryParameters), pathParameters);

    try {
        let exchanges = [
            "FTX",
        ];
        const currency = pathParameters.currency.toUpperCase();

        if (queryParameters != null && typeof queryParameters.exchanges !== "undefined") {
            exchanges = queryParameters.exchanges;
        }

        /**
         *
         * @type ExchangeManager
         */
        const manager = container.get(TYPES.ExchangeManager);
        const futures  = await manager.futureContractRead(currency, exchanges);

        await App.TearDown();

        return {
            statusCode: 200,
            body: JSON.stringify(futures),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        }
    }
    catch (e) {
        console.log(e);
        return {
            statusCode: 400,
            body: e.message,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
        }
    }
}
