const App = require("../src/bootstrap");
const { TYPES } = require("../src/utils")

/**
 * Method to get ticker from desired Exchange
 */
module.exports.index = async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    const container = await App.Start();
    const pathParameters = event.pathParameters;
    const queryParameters = event.multiValueQueryStringParameters;

    // console.log('order-book: start to get from Exchange', JSON.stringify(queryParameters), pathParameters);

    try {
        let exchanges = [
            "BN", "BCM", "BTCT", "BPR", "BRCF", "MB", "NVD",
        ];
        let limit = 10;

        if (queryParameters != null && typeof queryParameters.exchanges !== "undefined") {
            exchanges = queryParameters.exchanges;
        }

        if (queryParameters != null && typeof queryParameters.limit !== "undefined") {
            limit = queryParameters.limit;
        }

        const [base, quote] = pathParameters.symbol.toUpperCase().split("_");
        const symbol = `${base}_${quote}`;
        const manager = container.get(TYPES.ExchangeManager);
        /**
         * @type ExchangeManagerWriter
         */
        const exchangeManagerWriter = container.get(TYPES.ExchangeManagerWriter);
        const orderBooks = await manager.orderBookRead(symbol, exchanges, limit);

        // await exchangeManagerWriter.orderBookWrite(orderBooks, "http");

        return {
            statusCode: 200,
            body: JSON.stringify(orderBooks),
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
        }
    }
    catch (e) {
        console.log(e);
        return {
            statusCode: 400,
            body: e.message,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
        }
    }
}
