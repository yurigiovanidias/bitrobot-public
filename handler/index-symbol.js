const App = require("../src/bootstrap");
const { CONSTANT, TYPES } = require("../src/utils")

/**
 * Method to get index from desired Symbol
 */
module.exports.index = async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    const container = await App.Start();
    const pathParameters = event.pathParameters;
    const queryParameters = event.queryStringParameters;

    // console.log('index-symbol: start to get index symbol', pathParameters);

    try {
        const [base, quote] = pathParameters.symbol.toUpperCase().split("_");
        let exchanges = [
            CONSTANT.BITROBOT.ACRONYM,
            CONSTANT.AWESOMEAPI.ACRONYM,
            CONSTANT.BITMEX.ACRONYM,
            CONSTANT.FTX.ACRONYM,
            CONSTANT.EMX.ACRONYM,
        ];

        if (queryParameters != null && typeof queryParameters.exchanges !== "undefined") {
            exchanges = queryParameters.exchanges;
        }

        const symbol = `${base}_${quote}`;
        const exchangeManagerReader = container.get(TYPES.ExchangeManager);
        /**
         * @type ExchangeManagerWriter
         */
        const exchangeManagerWriter = container.get(TYPES.ExchangeManagerWriter);
        const cpExchanges = JSON.parse(JSON.stringify(exchanges));
        let [indexes, newIndexes] = await exchangeManagerReader.indexSymbolRead(symbol, cpExchanges);

        if (typeof newIndexes !== "undefined" && newIndexes.length > 0) {
            await exchangeManagerWriter.indexSymbolWrite(newIndexes, "http");
            indexes = indexes.concat(newIndexes);
        }

        const finalIndex = [];

        for (const exchange of exchanges) {
            const index = indexes.find(index => index.exchange === exchange);

            if (!index) {
                continue;
            }

            finalIndex.push(index);
        }

        await App.TearDown();

        return {
            statusCode: 200,
            body: JSON.stringify(finalIndex),
        }
    }
    catch (e) {
        console.log(e);
        return {
            statusCode: 400,
            body: e.message,
        }
    }
}
