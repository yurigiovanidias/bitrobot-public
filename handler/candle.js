const App = require("../src/bootstrap");
const { TYPES } = require("../src/utils");
const mapQueryString = require("amazon-api-gateway-querystring");

/**
 * Method to get candle from desired Exchange
 */
module.exports.index = async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    const container = await App.Start();
    const pathParameters = event.pathParameters;
    const queryParameters = event.multiValueQueryStringParameters;

    // console.log('candle: start to get from Exchange', JSON.stringify(queryParameters), pathParameters);

    try {
        const interval = queryParameters.interval;
        const startTime = (new Date(queryParameters.start_date)).getTime();
        const endTime = (new Date(queryParameters.end_date)).getTime();
        const exchange = queryParameters.exchange;

        const [base, quote] = pathParameters.symbol.toUpperCase().split("_");
        const symbol = `${base}_${quote}`;
        /**
         *
         * @type ExchangeManager
         */
        const manager = container.get(TYPES.ExchangeManager);
        /**
         * @type ExchangeManagerWriter
         */
        const exchangeManagerWriter = container.get(TYPES.ExchangeManagerWriter);
        let candles = await manager.candleRead(symbol, interval, startTime, endTime, exchange);

        // if (typeof newTickers !== "undefined" && newTickers.length > 0) {
        //     // await exchangeManagerWriter.candleWrite(newTickers, "http");
        //     candles = candles.concat(newTickers);
        // }

        await App.TearDown();

        return {
            statusCode: 200,
            body: JSON.stringify(candles),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        }
    }
    catch (e) {
        console.log(e);
        return {
            statusCode: 400,
            body: e.message,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
        }
    }
}
